//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#endif
class PayRoad
{
	private:
		int price = 500;
        int Cars, Cash, NonCash;
	public:
		int getCash(){
			return Cash;
		}
		int getNonCash(){
			return NonCash;
		}
		PayRoad() {Cars = 0, Cash = 0, NonCash = 0;}
		PayRoad Paying(PayRoad Road, bool cash);
};