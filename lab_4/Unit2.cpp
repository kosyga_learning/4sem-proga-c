//---------------------------------------------------------------------------

#pragma hdrstop

#include "Unit2.h"
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
PayRoad PayRoad::Paying(PayRoad Road, bool cash)
{
	Form1->ProgressBar1->Position = 0;
	Road.Cars ++;

	if(cash)
	{
		Road.Cash += Road.price;
		Form1->LabeledEdit1->Text = StrToInt(Form1->LabeledEdit1->Text) + 1;
	}
	else
	{
		Road.NonCash += Road.price;
		Form1->LabeledEdit2->Text = StrToInt(Form1->LabeledEdit2->Text) + 1;
	}
    Form1->ProgressBar1->Position = 100;
	return Road;
}
