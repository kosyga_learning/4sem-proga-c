object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 434
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 135
    Width = 299
    Height = 185
    BiDiMode = bdLeftToRight
    Caption = #1053#1072#1083#1080#1095#1085#1099#1077' '#1087#1083#1072#1090#1077#1078#1080
    ParentBiDiMode = False
    TabOrder = 0
    object LabeledEdit1: TLabeledEdit
      Left = 3
      Top = 40
      Width = 293
      Height = 21
      Alignment = taCenter
      EditLabel.Width = 84
      EditLabel.Height = 13
      EditLabel.Caption = #1055#1088#1086#1077#1093#1072#1083#1086' '#1084#1072#1096#1080#1085
      ReadOnly = True
      TabOrder = 0
      Text = '0'
    end
    object Button1: TButton
      Left = 3
      Top = 67
      Width = 293
      Height = 102
      Caption = #1042#1099#1088#1091#1095#1082#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 48
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = Button1Click
    end
  end
  object Button2: TButton
    Left = 152
    Top = 326
    Width = 313
    Height = 100
    Caption = #1054#1073#1097#1072#1103' '#1074#1099#1088#1091#1095#1082#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = 36
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = Button2Click
  end
  object GroupBox2: TGroupBox
    Left = 336
    Top = 135
    Width = 299
    Height = 185
    BiDiMode = bdLeftToRight
    Caption = #1041#1077#1079#1085#1072#1083#1080#1095#1085#1099#1077' '#1087#1083#1072#1090#1077#1078#1080
    ParentBiDiMode = False
    TabOrder = 2
    object LabeledEdit2: TLabeledEdit
      Left = 3
      Top = 40
      Width = 293
      Height = 21
      Alignment = taCenter
      EditLabel.Width = 84
      EditLabel.Height = 13
      EditLabel.Caption = #1055#1088#1086#1077#1093#1072#1083#1086' '#1084#1072#1096#1080#1085
      ReadOnly = True
      TabOrder = 0
      Text = '0'
    end
    object Button3: TButton
      Left = 3
      Top = 67
      Width = 293
      Height = 102
      Caption = #1042#1099#1088#1091#1095#1082#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 48
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = Button3Click
    end
  end
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 8
    Width = 635
    Height = 121
    BarColor = clBlue
    BackgroundColor = clBlue
    TabOrder = 3
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 512
    Top = 376
  end
end
