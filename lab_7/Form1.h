#pragma once
#include "Reis.h"

namespace Laba7 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::DataGridView^ dataGridView1;
	private: System::Windows::Forms::Button^ Add;

	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::TextBox^ Type;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::TextBox^ Count;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::TextBox^ Price;




	private: System::Windows::Forms::Button^ Save;
	private: System::Windows::Forms::Button^ Load;

	private: System::Windows::Forms::OpenFileDialog^ openFileDialog1;
	private: System::Windows::Forms::SaveFileDialog^ saveFileDialog1;
	private: System::Windows::Forms::GroupBox^ groupBox1;
	private: System::Windows::Forms::TextBox^ Way;
	private: System::Windows::Forms::Button^ getQeue;

	private: System::Windows::Forms::Label^ label5;

	private: System::Windows::Forms::DataGridViewTextBoxColumn^ Column1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ Column2;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ Column3;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ Column4;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ Column5;
	private: System::Windows::Forms::DateTimePicker^ Departure;




	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle5 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle6 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->Column1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column3 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column4 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column5 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Add = (gcnew System::Windows::Forms::Button());
			this->Way = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->Type = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->Count = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->Price = (gcnew System::Windows::Forms::TextBox());
			this->Save = (gcnew System::Windows::Forms::Button());
			this->Load = (gcnew System::Windows::Forms::Button());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->Departure = (gcnew System::Windows::Forms::DateTimePicker());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->getQeue = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->groupBox1->SuspendLayout();
			this->SuspendLayout();
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToResizeColumns = false;
			this->dataGridView1->AllowUserToResizeRows = false;
			this->dataGridView1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView1->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::AllCells;
			this->dataGridView1->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->dataGridView1->CellBorderStyle = System::Windows::Forms::DataGridViewCellBorderStyle::SingleHorizontal;
			this->dataGridView1->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::None;
			dataGridViewCellStyle4->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle4->BackColor = System::Drawing::SystemColors::ActiveCaption;
			dataGridViewCellStyle4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle4->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle4->SelectionBackColor = System::Drawing::SystemColors::ActiveCaption;
			dataGridViewCellStyle4->SelectionForeColor = System::Drawing::SystemColors::MenuHighlight;
			dataGridViewCellStyle4->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->dataGridView1->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
				this->Column1,
					this->Column2, this->Column3, this->Column4, this->Column5
			});
			dataGridViewCellStyle5->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle5->BackColor = System::Drawing::SystemColors::InactiveCaption;
			dataGridViewCellStyle5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle5->ForeColor = System::Drawing::SystemColors::ActiveCaptionText;
			dataGridViewCellStyle5->SelectionBackColor = System::Drawing::SystemColors::InactiveCaption;
			dataGridViewCellStyle5->SelectionForeColor = System::Drawing::SystemColors::MenuHighlight;
			dataGridViewCellStyle5->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
			this->dataGridView1->DefaultCellStyle = dataGridViewCellStyle5;
			this->dataGridView1->GridColor = System::Drawing::SystemColors::AppWorkspace;
			this->dataGridView1->ImeMode = System::Windows::Forms::ImeMode::NoControl;
			this->dataGridView1->Location = System::Drawing::Point(12, 123);
			this->dataGridView1->MultiSelect = false;
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::None;
			dataGridViewCellStyle6->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle6->BackColor = System::Drawing::SystemColors::InactiveCaption;
			dataGridViewCellStyle6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle6->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle6->SelectionBackColor = System::Drawing::SystemColors::ActiveCaption;
			dataGridViewCellStyle6->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle6->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->dataGridView1->RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
			this->dataGridView1->RowHeadersVisible = false;
			this->dataGridView1->RowHeadersWidth = 25;
			this->dataGridView1->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->dataGridView1->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView1->Size = System::Drawing::Size(535, 204);
			this->dataGridView1->TabIndex = 0;
			this->dataGridView1->UserDeletingRow += gcnew System::Windows::Forms::DataGridViewRowCancelEventHandler(this, &Form1::dataGridView1_UserDeletingRow);
			// 
			// Column1
			// 
			this->Column1->HeaderText = L"�������";
			this->Column1->Name = L"Column1";
			this->Column1->ReadOnly = true;
			// 
			// Column2
			// 
			this->Column2->HeaderText = L"������";
			this->Column2->Name = L"Column2";
			this->Column2->ReadOnly = true;
			// 
			// Column3
			// 
			this->Column3->HeaderText = L"���-��";
			this->Column3->Name = L"Column3";
			this->Column3->ReadOnly = true;
			// 
			// Column4
			// 
			this->Column4->HeaderText = L"����";
			this->Column4->Name = L"Column4";
			this->Column4->ReadOnly = true;
			// 
			// Column5
			// 
			this->Column5->HeaderText = L"���� ������";
			this->Column5->Name = L"Column5";
			this->Column5->ReadOnly = true;
			// 
			// Add
			// 
			this->Add->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->Add->Location = System::Drawing::Point(19, 68);
			this->Add->Name = L"Add";
			this->Add->Size = System::Drawing::Size(97, 23);
			this->Add->TabIndex = 1;
			this->Add->Text = L"add";
			this->Add->UseVisualStyleBackColor = true;
			this->Add->Click += gcnew System::EventHandler(this, &Form1::Add_Click);
			// 
			// Way
			// 
			this->Way->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->Way->Location = System::Drawing::Point(19, 42);
			this->Way->Name = L"Way";
			this->Way->Size = System::Drawing::Size(97, 20);
			this->Way->TabIndex = 2;
			this->Way->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->Way->WordWrap = false;
			this->Way->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::Way_KeyPress);
			// 
			// label1
			// 
			this->label1->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(45, 23);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(52, 13);
			this->label1->TabIndex = 3;
			this->label1->Text = L"�������";
			// 
			// label2
			// 
			this->label2->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(151, 23);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(51, 13);
			this->label2->TabIndex = 5;
			this->label2->Text = L"������";
			// 
			// Type
			// 
			this->Type->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->Type->Location = System::Drawing::Point(132, 42);
			this->Type->Name = L"Type";
			this->Type->Size = System::Drawing::Size(80, 20);
			this->Type->TabIndex = 4;
			this->Type->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->Type->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::Type_KeyPress);
			// 
			// label3
			// 
			this->label3->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(227, 23);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(41, 13);
			this->label3->TabIndex = 7;
			this->label3->Text = L"���-��";
			// 
			// Count
			// 
			this->Count->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->Count->Location = System::Drawing::Point(218, 42);
			this->Count->Name = L"Count";
			this->Count->Size = System::Drawing::Size(75, 20);
			this->Count->TabIndex = 6;
			this->Count->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->Count->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::Count_KeyPress);
			// 
			// label4
			// 
			this->label4->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(322, 23);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(33, 13);
			this->label4->TabIndex = 9;
			this->label4->Text = L"����";
			// 
			// Price
			// 
			this->Price->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->Price->Location = System::Drawing::Point(299, 42);
			this->Price->Name = L"Price";
			this->Price->Size = System::Drawing::Size(82, 20);
			this->Price->TabIndex = 8;
			this->Price->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->Price->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::Price_KeyPress);
			// 
			// Save
			// 
			this->Save->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->Save->Location = System::Drawing::Point(132, 68);
			this->Save->Name = L"Save";
			this->Save->Size = System::Drawing::Size(129, 23);
			this->Save->TabIndex = 10;
			this->Save->Text = L"Save";
			this->Save->UseVisualStyleBackColor = true;
			this->Save->Click += gcnew System::EventHandler(this, &Form1::Save_Click);
			// 
			// Load
			// 
			this->Load->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->Load->Location = System::Drawing::Point(267, 68);
			this->Load->Name = L"Load";
			this->Load->Size = System::Drawing::Size(116, 23);
			this->Load->TabIndex = 11;
			this->Load->Text = L"Load";
			this->Load->UseVisualStyleBackColor = true;
			this->Load->Click += gcnew System::EventHandler(this, &Form1::Load_Click);
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			this->openFileDialog1->Filter = L"\"Text files (*.txt)|*.TXT|Pascal files (*.pas)|*.PAS\"";
			// 
			// saveFileDialog1
			// 
			this->saveFileDialog1->DereferenceLinks = false;
			this->saveFileDialog1->Filter = L"\"Text files (*.txt)|*.TXT|Pascal files (*.pas)|*.PAS\"";
			// 
			// groupBox1
			// 
			this->groupBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBox1->Controls->Add(this->Departure);
			this->groupBox1->Controls->Add(this->label5);
			this->groupBox1->Controls->Add(this->getQeue);
			this->groupBox1->Controls->Add(this->Price);
			this->groupBox1->Controls->Add(this->Load);
			this->groupBox1->Controls->Add(this->Save);
			this->groupBox1->Controls->Add(this->Way);
			this->groupBox1->Controls->Add(this->Add);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Controls->Add(this->Type);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->Count);
			this->groupBox1->Location = System::Drawing::Point(12, 3);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(535, 110);
			this->groupBox1->TabIndex = 12;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"����";
			// 
			// Departure
			// 
			this->Departure->Location = System::Drawing::Point(387, 42);
			this->Departure->Name = L"Departure";
			this->Departure->Size = System::Drawing::Size(142, 20);
			this->Departure->TabIndex = 15;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(421, 16);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(80, 13);
			this->label5->TabIndex = 14;
			this->label5->Text = L"����� ������";
			// 
			// getQeue
			// 
			this->getQeue->Location = System::Drawing::Point(387, 68);
			this->getQeue->Name = L"getQeue";
			this->getQeue->Size = System::Drawing::Size(142, 23);
			this->getQeue->TabIndex = 12;
			this->getQeue->Text = L"������� �������";
			this->getQeue->UseVisualStyleBackColor = true;
			this->getQeue->Click += gcnew System::EventHandler(this, &Form1::getQeue_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::AppWorkspace;
			this->ClientSize = System::Drawing::Size(565, 339);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->dataGridView1);
			this->Name = L"Form1";
			this->Text = L"Laba7";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
	MyReisBase* ReisBase = new MyReisBase;
	private: void Show(std::vector<Reis> Base)
	{
		dataGridView1->Rows->Clear();
		for each (auto reis in Base)
		{
			dataGridView1->Rows->Add(gcnew String(reis.getWay()), gcnew String(reis.getType()), reis.getCount(), reis.getPrice(), reis.getDeparture().toString());
		}
	}
	private: System::Void Add_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		if (Way->Text == "" || Type->Text == "" || Count->Text == "" || Price->Text == "") 
		{
			MessageBox::Show("�� �� ����� ��� ��������");
			return;
		}
		ReisBase->UploadBase(Reis(Way->Text,Type->Text,Count->Text,Price->Text,Departure->Value));
		Show(ReisBase->getValue());
		Way->Clear();
		Type->Clear();
		Count->Clear();
		Price->Clear();
	}
	private: System::Void Save_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		saveFileDialog1->ShowDialog();
		if (saveFileDialog1->FileName) 
		{
			System::IntPtr ptrToNativeString = Marshal::StringToHGlobalAnsi(saveFileDialog1->FileName); //������ AnsiString � MVS
			char* path = static_cast<char*>(ptrToNativeString.ToPointer());
			ReisBase->Serialize(path);
		}
	}
	private: System::Void Load_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		openFileDialog1->ShowDialog();
		if (openFileDialog1->FileName)
		{
			dataGridView1->Rows->Clear();
			System::IntPtr ptrToNativeString = Marshal::StringToHGlobalAnsi(openFileDialog1->FileName); //������ AnsiString � MVS
			char* path = static_cast<char*>(ptrToNativeString.ToPointer());
			ReisBase = new MyReisBase(path);
		}
		Show(ReisBase->getValue());
	}
	private: System::Void Count_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		if ((!isdigit(e->KeyChar)) && (e->KeyChar != '\b')) e->KeyChar = 0;
	}
	private: System::Void Price_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		if ((!isdigit(e->KeyChar)) && (e->KeyChar != '\b')) e->KeyChar = 0;
	}
	private: System::Void dataGridView1_UserDeletingRow(System::Object^ sender, System::Windows::Forms::DataGridViewRowCancelEventArgs^ e) 
	{
		//Reis toDel = Reis(dataGridView1[0, e->Row->Index]->Value->ToString(), dataGridView1[1, e->Row->Index]->Value->ToString(), dataGridView1[2, e->Row->Index]->Value->ToString(), dataGridView1[3, e->Row->Index]->Value->ToString(), dataGridView1[4, e->Row->Index]->Value->ToString());
		//ReisBase->delReis(toDel);
	}
	private: System::Void Way_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		if (e->KeyChar == '|') e->KeyChar = 0;
	}
	private: System::Void Type_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		if (e->KeyChar == '|') e->KeyChar = 0;
	}
	private: System::Void getQeue_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		MessageBox::Show(ReisBase->getQeue());
	}
};
}
