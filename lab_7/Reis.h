#pragma once
#include <iostream>
#include <string> 
#include <string.h>
#include <vector>
#include <fstream>
using namespace System::Runtime::InteropServices;
class date {
private:
	int day, month, year;
public:
	date() {};
	date(int day, int month, int year) {
		this->day = day;
		this->month = month;
		this->year = year;
	}
	bool operator<(date date) {
		if (date.year == this->year)
			if (date.month == this->month)
				return date.day > this->day;
			else return date.month > this->month;
		else return date.year > this->year;
	}
	System::String^ toString() {
		System::String^ text = day + "." + month + "." + year;
		return text;
	}
};
struct qeue {
	date dates[100];
	int i = -1;
	void push(date date) {
		i++;
		dates[i] = date;
	}
	date top() {
		return dates[i];
	}
	void pop() {
		i--;
	}
	bool empty() {
		return i == -1;
	}
	void sort() {
		for (int i = 0; i < this->i+1; i++) {
			for (int j = 0; j < this->i; j++) {
				if (dates[j] < dates[j + 1]) {
					date b = dates[j]; // ������� �������������� ����������
					dates[j] = dates[j + 1]; // ������ �������
					dates[j + 1] = b; // �������� ���������
				}
			}
		}
	}
};
class Reis
{
private:
	static const short int len = 128;
	char way[len];
	char type[len];
	int count;
	int price;
	date departure;

	char* ustoc(System::String^ str) 
	{
		System::IntPtr ptrToNativeString = Marshal::StringToHGlobalAnsi(str); //������ AnsiString � MVS
		return static_cast<char*>(ptrToNativeString.ToPointer());
	}
public:
	Reis(System::String^ w, System::String^ t, System::String^ c, System::String^ p, System::DateTime d)
	{
		strcpy(way, ustoc(w));
		strcpy(type, ustoc(t));
		this->count = std::stoi(ustoc(c));
		this->price = std::stoi(ustoc(p));
		this->departure = date(d.Day, d.Month, d.Year);
	}
	Reis(char* w, char* t, char* c, char* p) 
	{ 
		strcpy(way, w); 
		strcpy(type, t);
		count = std::stoi(c);
		price = std::stoi(p);
	}
	char* getWay() { return way; }
	char* getType() { return type; }
	int getCount() { return count; }
	int getPrice() { return price; }
	date getDeparture() { return departure; }
	bool operator == (Reis value)
	{
		return ((std::string(value.way) == std::string(way))
			&& (std::string(value.type) == std::string(type)) 
			&& (value.count == count) 
			&& (value.price == price));
	}
};

class MyReisBase {
private:
	std::vector<Reis> Base;
	qeue qeue;
	char separator = '|';

	std::vector<std::string> split(std::string str)
	{
		std::vector<std::string> values(1);
		for each (char sym in str)
		{
			if (sym != separator) values[values.size()-1] += sym;
			else values.resize(values.size()+1);
		}
		return values;
	}
	void setQeue() {
		for each (Reis reis in Base)
			qeue.push(reis.getDeparture());
	}
public:
	MyReisBase() {};
	void UploadBase(Reis R) { Base.push_back(R);}
	std::vector<Reis> getValue() { return Base; }
	void Serialize(char *path)
	{
		std::ofstream fout(path);
		for each (auto reise in Base)
		{
			fout << std::string(reise.getWay()) + separator 
				+ std::string(reise.getType()) + separator
				+ std::to_string(reise.getCount()) + separator
				+ std::to_string(reise.getPrice()) + "\n";
		}
		fout.close();
	}
	MyReisBase(char *path)
	{
		std::ifstream fin(path);
		std::string str;
		while (std::getline(fin, str))
		{
			std::vector<std::string> values = split(str);
			Base.push_back(Reis(strdup(values[0].c_str()), strdup(values[1].c_str()), strdup(values[2].c_str()), strdup(values[3].c_str())));
		}
		fin.close();
	}
	void delReis(Reis value) 
	{
		for (std::vector<Reis>::iterator iter = Base.begin(); iter != Base.end(); ++iter)
			if (*iter == value)
			{
				Base.erase(iter);
				break;
			}
	}
	System::String^ getQeue() 
	{
		setQeue();
		qeue.sort();
		System::String^ text = "";
		int i = 1;
		while (!qeue.empty()) {
			text += i + " - " + qeue.top().toString() + "\n";
			qeue.pop();
			i++;
		}
		return text;
	}
};