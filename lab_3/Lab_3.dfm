object Form1: TForm1
  Left = 0
  Top = 0
  Caption = #1051'/'#1088' 3'
  ClientHeight = 232
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 8
    Width = 47
    Height = 16
    Caption = #1042#1088#1077#1084#1103' 1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 280
    Top = 8
    Width = 47
    Height = 16
    Caption = #1042#1088#1077#1084#1103' 2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Memo1: TMemo
    Left = 8
    Top = 72
    Width = 449
    Height = 89
    ReadOnly = True
    TabOrder = 0
  end
  object Button1: TButton
    Left = 144
    Top = 184
    Width = 137
    Height = 25
    Caption = #1042#1099#1095#1080#1089#1083#1080#1090#1100
    TabOrder = 1
    OnClick = Button1Click
  end
  object TimePicker1: TTimePicker
    Left = 24
    Top = 34
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    TabOrder = 2
    TimeFormat = 'hh:mm:ss'
  end
  object TimePicker2: TTimePicker
    Left = 280
    Top = 34
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    TabOrder = 3
    TimeFormat = 'hh:mm:ss'
  end
  object Button2: TButton
    Left = 280
    Top = 184
    Width = 137
    Height = 25
    Caption = #1042#1099#1074#1077#1089#1090#1080' '#1087#1077#1088#1074#1086#1077
    TabOrder = 4
    Visible = False
    OnClick = Button2Click
  end
end
