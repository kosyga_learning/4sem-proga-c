//---------------------------------------------------------------------------

#ifndef Lab_3H
#define Lab_3H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.WinXPickers.hpp>
#include <ctime>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.WinXPickers.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TMemo *Memo1;
	TButton *Button1;
	TTimePicker *TimePicker1;
	TTimePicker *TimePicker2;
	TLabel *Label1;
	TLabel *Label2;
	TButton *Button2;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};

class MyTime {
	public:
		unsigned short hour, min, sec;
		MyTime() {hour = 0; min = 0; sec = 0;}
		MyTime(unsigned short h, unsigned short m, unsigned short s) {hour = h; min = m; sec = s;}
		void show(MyTime t);
		MyTime sum(MyTime t1, MyTime t2);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
