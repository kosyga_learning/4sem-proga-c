//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Lab_3.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

void MyTime::show(MyTime t) {
	Form1->Memo1->Text = IntToStr(t.hour) + ":" + IntToStr(t.min) + ":" + IntToStr(t.sec);
}

MyTime MyTime::sum(MyTime t1, MyTime t2) {
	sec = t1.sec + t2.sec;
	min = t1.min + t2.min;
	hour = t1.hour + t2.hour;
	if(sec>=60){min++; sec-=60;}
	if(min>=60){hour++; min-=60;}
	if(hour>=24)hour-=24;
	MyTime t(hour,min,sec);
	return t;
}
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	Memo1->Clear();
	MyTime T1, T2, T3;
	unsigned short ms;
	TimePicker1->Time.DecodeTime(&T1.hour, &T1.min, &T1.sec, &ms);
	TimePicker2->Time.DecodeTime(&T2.hour, &T2.min, &T2.sec, &ms);
	T3 = T3.sum(T1, T2);
	T3.show(T3);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	MyTime T;
	unsigned short ms;
	TimePicker1->Time.DecodeTime(&T.hour, &T.min, &T.sec, &ms);
    T.show(T);
}
//---------------------------------------------------------------------------

