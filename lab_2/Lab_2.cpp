//---------------------------------------------------------------------------

#include <vcl.h>
#include <vector>
#include "windows.h"

#pragma hdrstop

#include "Lab_2.h"
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

using namespace std;

vector<int> arr;

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
    clearImage();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	arr.clear();
	AnsiString text;
	for(int i = 0; i < 20; i++)
	{
		arr.push_back(rand()%100);
		text+= IntToStr(arr[i]) + "\r\n";
	}
	Memo1->Text = text;
	Draw(-1,arr);
	Button2->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
    Button1->Enabled = false;
	Button2->Enabled = false;

	switch(ComboBox1->ItemIndex)
	{
		case 0: BubbleSort(arr);
			break;
		case 1: ShakerSort(arr);
			break;
		case 2: CombSort(arr);
			break;
		case 3: InsertionSort(arr);
			break;
		case 4: SelectionSort(arr);
			break;
		case 5: QuickSort(arr);
			break;
		case 6: MergeSort(arr);
			break;
		case 7: HeapSort(arr);
			break;
	}
	Draw(-1,arr);
	Button1->Enabled = true;
}
//---------------------------------------------------------------------------


