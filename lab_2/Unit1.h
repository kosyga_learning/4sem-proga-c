//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <vector>
#include <algorithm>
#include "windows.h"

using std::vector;

void clearImage();

void Draw(int t, vector<int> arr);

void BubbleSort(vector<int>& values);

void ShakerSort(vector<int>& values);

void CombSort(vector<int>& values);

void InsertionSort(vector<int>& values);

void SelectionSort(vector<int>& values);

void MergeSort(vector<int>& values);

void HeapSort(vector<int>& values);

void QuickSort(vector<int>& values);

#endif
