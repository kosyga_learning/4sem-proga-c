//---------------------------------------------------------------------------
#pragma hdrstop

#include "Unit1.h"
#include "Lab_2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

using namespace std;

void clearImage() {
	Form1->Image1->Canvas->Brush->Color = clWhite;
	Form1->Image1->Canvas->FillRect(Rect(0,0, Form1->Image1->Width, Form1->Image1->Height));
}

/*void Draw(int t) {
	const int iH = Form1->Image1->Height;
	const int r = iH/Max;
	const int w = 40;
	clearImage();
	for (int i = 0; i < myVector.size(); i++) {
		Form1->Image1->Canvas->Brush->Color = clWhite;
		Form1->Image1->Canvas->TextOutW(10+i*w, (iH-myVector[i]*r)-20, IntToStr(myVector[i]));
		Form1->Image1->Canvas->Brush->Color = clGray;
        if (i==t) Form1->Image1->Canvas->Brush->Color = clRed;
		Form1->Image1->Canvas->Rectangle(8+i*w, iH-myVector[i]*r, 1.1*w+i*w, iH);
	}
} */

void BubbleSort(vector<int>& values) {
  for (size_t idx_i = 0; idx_i + 1 < values.size(); ++idx_i) {
	for (size_t idx_j = 0; idx_j + 1 < values.size() - idx_i; ++idx_j) {
	  if (values[idx_j + 1] < values[idx_j]) {
		swap(values[idx_j], values[idx_j + 1]);
        Application->ProcessMessages();
		Sleep(100);
		Draw(idx_j+1, values);
	  }
	}
  }
}

void ShakerSort(vector<int>& values) {
  if (values.empty()) {
	return;
  }
  int left = 0;
  int right = values.size() - 1;
  while (left <= right) {
	for (int i = right; i > left; --i) {
	  if (values[i - 1] > values[i]) {
		swap(values[i - 1], values[i]);
		Application->ProcessMessages();
		Sleep(100);
		Draw(i-1, values);
      }
    }
	++left;
    for (int i = left; i < right; ++i) {
      if (values[i] > values[i + 1]) {
		swap(values[i], values[i + 1]);
        Application->ProcessMessages();
		Sleep(100);
		Draw(i+1, values);
      }
    }
	--right;
  }
}

void CombSort(vector<int>& values) {
  const double factor = 1.247; // ������ ����������
  double step = values.size() - 1;

  while (step >= 1) {
	for (int i = 0; i + step < values.size(); ++i) {
      if (values[i] > values[i + step]) {
		swap(values[i], values[i + step]);
        Application->ProcessMessages();
		Sleep(150);
		Draw(i+step, values);
	  }
	}
	step /= factor;
  }
  // ���������� ���������
  for (size_t idx_i = 0; idx_i + 1 < values.size(); ++idx_i) {
    for (size_t idx_j = 0; idx_j + 1 < values.size() - idx_i; ++idx_j) {
      if (values[idx_j + 1] < values[idx_j]) {
		swap(values[idx_j], values[idx_j + 1]);
        Application->ProcessMessages();
		Sleep(150);
		Draw(idx_j+1, values);
	  }
	}
  }
}

void InsertionSort(vector<int>& values) {
  for (size_t i = 1; i < values.size(); ++i) {
    int x = values[i];
	size_t j = i;
    while (j > 0 && values[j - 1] > x) {
      values[j] = values[j - 1];
	  --j;
      Application->ProcessMessages();
	  Sleep(100);
	  Draw(j-1, values);
	}
	values[j] = x;
  }
}

void SelectionSort(vector<int>& values) {
  for (auto i = values.begin(); i != values.end(); ++i) {
	auto j = min_element(i, values.end());
	swap(*i, *j);
    Application->ProcessMessages();
	Sleep(150);
	Draw(*i, values);
  }
}

void MergeSortImpl(vector<int>& values, vector<int>& buffer, int l, int r) {
  if (l < r) {
    int m = (l + r) / 2;
    MergeSortImpl(values, buffer, l, m);
	MergeSortImpl(values, buffer, m + 1, r);

    int k = l;
    for (int i = l, j = m + 1; i <= m || j <= r; ) {
	  if (j > r || (i <= m && values[i] < values[j])) {
        buffer[k] = values[i];
		++i;
        Application->ProcessMessages();
		Sleep(100);
		Draw(j, values);
	  }
	  else {
		buffer[k] = values[j];
        ++j;
      }
      ++k;
	}
    for (int i = l; i <= r; ++i) {
      values[i] = buffer[i];
	}
  }
}

void MergeSort(vector<int>& values) {
  if (!values.empty()) {
    vector<int> buffer(values.size());
	MergeSortImpl(values, buffer, 0, values.size() - 1);
  }
}

void HeapSort(vector<int>& values) {
	make_heap(values.begin(), values.end());
	for (auto i = values.end(); i != values.begin(); --i) {
		pop_heap(values.begin(), i);
        Application->ProcessMessages();
		Sleep(200);
		Draw(*i, values);
	}
}

int Partition(vector<int>& values, int l, int r) {
  int x = values[r];
  int less = l;

  for (int i = l; i < r; ++i) {
    if (values[i] <= x) {
      swap(values[i], values[less]);
	  ++less;
      Application->ProcessMessages();
	  Sleep(100);
	  Draw(i, values);
    }
  }
  swap(values[less], values[r]);
  return less;
}

void QuickSortImpl(vector<int>& values, int l, int r) {
  if (l < r) {
    int q = Partition(values, l, r);
    QuickSortImpl(values, l, q - 1);
    QuickSortImpl(values, q + 1, r);
  }
}

void QuickSort(vector<int>& values) {
  if (!values.empty()) {
    QuickSortImpl(values, 0, values.size() - 1);
  }
}
void Draw(int t, vector<int> arr)
{
	int iH = Form1->Image1->Height;
	Form1->Image1->Picture->Bitmap->FreeImage();                   //������� ��������
	Form1->Image1->Picture->Bitmap = NULL;
	for(int i = 0; i < arr.size();i++)
	{
		Form1->Image1->Canvas->Brush->Color = clWhite;
		Form1->Image1->Canvas->TextOutW(10 + i * 40, (iH - arr[i]*iH/100)-20, IntToStr(arr[i]));
		Form1->Image1->Canvas->Brush->Color = clGray;
		if(i==t) Form1->Image1->Canvas->Brush->Color = clRed;
		Form1->Image1->Canvas->Rectangle(8+i*40,iH-arr[i]*iH/100,40*(1.1+i),iH);
	}
}

