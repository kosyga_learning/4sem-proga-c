﻿//---------------------------------------------------------------------------

#include <vcl.h>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------

cords mousePos; //переменная для координат мыши
rect newRect;   //экземпляр фигуры, которую мы собираемся нарисовать
rect t;         //перетаскиваемая фигура
int kCur=0;     //глобальный счётчик текущего положения в истории фигур
std::vector<rect> History;  //история фигур
Dmode mode;                 //текущий режим (обычный,рисование,движение)

__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	Image2->Canvas->Brush->Color = clBlack;      //окрашиваем индикатор цвета в дефолтный цвет
	Image2->Canvas->FillRect(Rect(0,0,Image2->Height,Image2->Width));

	mode = Dmode::None;//устанавливаем режим в обычный
}

//---------------------------------------------------------------------------

void __fastcall TForm1::Image2Click(TObject *Sender) //выбор цвета контура
{
	if(ColorDialog1->Execute()) newRect.color = ColorDialog1->Color;  //задаём экземпляру новой фугуры цвет контура
	Image2->Canvas->Brush->Color=newRect.color; //окрашиваем индикатор цвета в выбранный цвет
	Image2->Canvas->FillRect(Rect(0,0,Image2->Height,Image2->Width));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Image1MouseMove(TObject *Sender, TShiftState Shift, int X,   //движение мыши
          int Y)
{
	mousePos.x = X;      //записываем текущее положение мыши в глобальную переменную
	mousePos.y = Y;

	if(mode == Dmode::Drawing)
	{

	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
	newRect.first = mousePos;     //записываем текущие координаты мыши в координаты первой точки экземпляра новой фигуры
	int i = 0, j;    //переменные текущей области для отслеживания положения фигуры в истории

	History.resize(kCur);    //синхронизировать историю с текущим рисунком

	//проверяем попали ли мы в какую-нибудь фигуру
	for(auto rect : History)   //перебираем все фигуры в истории
	{
		bool strike = false;

		switch(rect.shape)
		{
			case shapes::Line:     //проверка на линию
				if (((mousePos.x - rect.first.x)/(rect.second.x - rect.first.x)==(mousePos.y - rect.first.y)/(rect.second.y - rect.first.y))) strike = true;
				break;
			case shapes::Square:   //проверка на прямоугольник
				if ((rect.first.x <= mousePos.x && mousePos.x <= rect.second.x)
				&& ( rect.first.y <= mousePos.y && mousePos.y <= rect.second.y)) strike = true;
				break;
			case shapes::Circle:   //проверка на эллипс
				if (1>=(pow(mousePos.x-0.5*(rect.first.x+rect.second.x), 2)/(pow((rect.first.x-rect.second.x), 2))
				 + (pow(mousePos.y-0.5*(rect.first.y+rect.second.y), 2)/(pow((rect.first.y-rect.second.y), 2))))) strike = true;
				break;
		}
		if(strike)         //если попали
		{
			t = rect;      //сохраняем фигуру в буфер
			j = i;         //сохраняем позицию в буфер
			mode = Dmode::Moveing;    //включаем режим перетаскивания
		}
		i++;
	}

	if(mode == Dmode::Moveing)   //если включился мод рисования
	{
		History.erase(History.begin() +	j);   //удаляем последнюю фигуру в истории, в которую мы попали
		drawTo(History.size());               //перерисовываем картинку без фигуры
	}
	else mode = Dmode::Drawing;               //иначе включаем режим рисования
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image1MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	newRect.second = mousePos;    //записываем текущие координаты мыши в координаты второй точки экземпляра новой фигуры

	if(mode == Dmode::Moveing)    //если стоит режим рисования
	{
		cords d {newRect.second.x - newRect.first.x, newRect.second.y - newRect.first.y};  //высчитываем изменение положения мыши
		t.first.x += d.x;
		t.first.y += d.y;                      //добавляем это изменение к перетаскиваемой фигуре
		t.second.x += d.x;
		t.second.y += d.y;
		draw(t);                  //рисуем её
		History.push_back(t);     //добавляем её в историю
	}
	else
	{
		draw(newRect);       //рисуем экземпляр новой фигуры
		History.push_back(newRect);      //добавляем его в историю
	}
	kCur = History.size();             //обновляем глобальный счётчик
	mode = Dmode::None;                //ставим обычный режим
}
//---------------------------------------------------------------------------

void __fastcall TForm1::RadioButton1Click(TObject *Sender)
{
	newRect.shape = shapes::Line;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::RadioButton2Click(TObject *Sender)
{
	newRect.shape = shapes::Square;           //выбираем фигуру
}
//---------------------------------------------------------------------------

void __fastcall TForm1::RadioButton3Click(TObject *Sender)
{
	newRect.shape = shapes::Circle;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit1KeyPress(TObject *Sender, System::WideChar &Key)
{
    if((!isdigit(Key))&(Key!='\b')) Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit1Change(TObject *Sender)
{
	if(Edit1->Text=="") Edit1->Text = "0";     //только цифры
	newRect.Width = StrToInt(Edit1->Text);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image3Click(TObject *Sender)
{
	if(ColorDialog1->Execute()) newRect.backCol = ColorDialog1->Color;     //задаём экземпляру новой фугуры цвет заливки
	Image3->Canvas->Brush->Color = newRect.backCol;                        //окрашиваем индикатор цвета в выбранный цвет
	Image3->Canvas->FillRect(Rect(0,0,Image3->Height,Image3->Width));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)     //очистить
{
	Image1->Picture->Bitmap->FreeImage();                   //очищаем картинку
	Image1->Picture->Bitmap = NULL;
	kCur = 0;                                              //удаляем историю
	History.clear();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)       //сохранить
{
	if(SaveDialog1->Execute())
	{
		ofstream fout(SaveDialog1->FileName.w_str(), ios::binary);  //открываем бинарный файл
		for(auto Rect: History)                                //перебираем все фигуры в истории
		{
			fout.write(reinterpret_cast<char*>(&Rect), sizeof(rect));  //записываем в файл каждую фигуру
		}
		fout.close();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)    //загрузить
{
	rect tmp;                  //объявляем временную буферную фигуру
	History.clear();           //очищаем историю
	if(OpenDialog1->Execute())
	{
		ifstream fin(OpenDialog1->FileName.w_str(), ios::binary);  //открываем бинарный файл
		while(!fin.eof())    //читаем его пока не конец файла
		{
			fin.read(reinterpret_cast<char*>(&tmp), sizeof(rect)); //записываем в буфер каждый элемент
			History.push_back(tmp);       //добавляем в историю временную буферную фигуру
		}

		kCur = History.size();      //обновляем глобальный счётчик
		drawTo(kCur);       //перерисовываем картинку по истории
	}
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button4Click(TObject *Sender)     //undo
{
	if(kCur==0) return;             //если мы на нулевой позиции в истории, то ничего не происходит
	kCur--;                         //иначе опускаемся на одну позицию
	drawTo(kCur);                   //и перерисовываем
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)    //redo
{
	if(kCur==History.size()) return;      //если мы на последней версии в истории, то ничего не происходит
	kCur++;                               //иначе поднимаемся на одну
	drawTo(kCur);                         //и перерисовываем
}

void __fastcall TForm1::drawTo(int to)    //перерисовать до заданной позиции
{
	Image1->Picture->Bitmap->FreeImage();       //очищаем картинку
	Image1->Picture->Bitmap = NULL;

	for (auto Rect : History) draw(Rect);  //перебираем все фигуры в истории и рисуем их
}
void __fastcall TForm1::draw(rect Rect)   //нарисовать
{
	Image1->Canvas->Pen->Color = Rect.color;     //выбираем кисти
	Image1->Canvas->Pen->Width = Rect.Width;
	Image1->Canvas->Brush->Color = Rect.backCol;

	switch(Rect.shape)              //выбираем фигуру и рисуем
	{
		case shapes::Line:
            Image1->Canvas->MoveTo(Rect.first.x, Rect.first.y);
			Image1->Canvas->LineTo(Rect.second.x, Rect.second.y);
			break;
		case shapes::Square:
			Image1->Canvas->Rectangle(Rect.first.x, Rect.first.y, Rect.second.x, Rect.second.y);
			break;
		case shapes::Circle:
			Image1->Canvas->Ellipse(Rect.first.x, Rect.first.y, Rect.second.x, Rect.second.y);
			break;
	}
}
//---------------------------------------------------------------------------

