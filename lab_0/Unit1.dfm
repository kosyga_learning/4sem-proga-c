object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 670
  ClientWidth = 862
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  DesignSize = (
    862
    670)
  TextHeight = 13
  object Image1: TImage
    Left = 8
    Top = 30
    Width = 726
    Height = 638
    Anchors = [akLeft, akTop, akRight, akBottom]
    OnMouseDown = Image1MouseDown
    OnMouseMove = Image1MouseMove
    OnMouseUp = Image1MouseUp
  end
  object Image2: TImage
    Left = 144
    Top = 1
    Width = 25
    Height = 25
    Anchors = [akTop]
    OnClick = Image2Click
  end
  object Label1: TLabel
    Left = 96
    Top = 1
    Width = 42
    Height = 23
    Anchors = [akTop]
    Caption = 'Color'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 6
    Top = 8
    Width = 28
    Height = 13
    Anchors = [akTop]
    Caption = 'Width'
  end
  object Image3: TImage
    Left = 175
    Top = 1
    Width = 25
    Height = 25
    Anchors = [akTop]
    OnClick = Image3Click
  end
  object RadioButton1: TRadioButton
    Left = 732
    Top = 32
    Width = 113
    Height = 17
    Anchors = [akTop, akRight]
    Caption = 'Line'
    Checked = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    TabStop = True
    OnClick = RadioButton1Click
  end
  object RadioButton2: TRadioButton
    Left = 732
    Top = 71
    Width = 113
    Height = 17
    Anchors = [akTop, akRight]
    Caption = 'Square'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = RadioButton2Click
  end
  object RadioButton3: TRadioButton
    Left = 732
    Top = 110
    Width = 113
    Height = 17
    Anchors = [akTop, akRight]
    Caption = 'Circle'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = RadioButton3Click
  end
  object Edit1: TEdit
    Left = 40
    Top = 5
    Width = 41
    Height = 21
    Anchors = [akTop]
    TabOrder = 3
    Text = '12'
    OnChange = Edit1Change
    OnKeyPress = Edit1KeyPress
  end
  object Button1: TButton
    Left = 455
    Top = 1
    Width = 75
    Height = 25
    Anchors = [akTop]
    Caption = 'Save'
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 536
    Top = 1
    Width = 75
    Height = 25
    Anchors = [akTop]
    Caption = 'Clear'
    TabOrder = 5
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 617
    Top = 1
    Width = 75
    Height = 25
    Anchors = [akTop]
    Caption = 'Load'
    TabOrder = 6
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 208
    Top = 1
    Width = 75
    Height = 25
    Anchors = [akTop]
    Caption = 'Undo'
    TabOrder = 7
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 289
    Top = 1
    Width = 75
    Height = 25
    Anchors = [akTop]
    Caption = 'Redo'
    TabOrder = 8
    OnClick = Button5Click
  end
  object ColorDialog1: TColorDialog
    Left = 792
    Top = 296
  end
  object SaveDialog1: TSaveDialog
    Filter = '|*.dat'
    Left = 792
    Top = 144
  end
  object OpenDialog1: TOpenDialog
    Filter = '|*.dat'
    Left = 792
    Top = 224
  end
end
