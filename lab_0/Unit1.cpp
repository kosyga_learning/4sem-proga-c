//---------------------------------------------------------------------------

#include <vcl.h>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------

cords mousePos; //���������� ��� ��������� ����
rect newRect;   //��������� ������, ������� �� ���������� ����������
rect t;         //��������������� ������
int kCur=0;     //���������� ������� �������� ��������� � ������� �����
std::vector<rect> History;  //������� �����
Dmode mode;                 //������� ����� (�������,���������,��������)

__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	Image2->Canvas->Brush->Color = clBlack;      //���������� ��������� ����� � ��������� ����
	Image2->Canvas->FillRect(Rect(0,0,Image2->Height,Image2->Width));

	mode = Dmode::None;//������������� ����� � �������
}

//---------------------------------------------------------------------------

void __fastcall TForm1::Image2Click(TObject *Sender) //����� ����� �������
{
	if(ColorDialog1->Execute()) newRect.color = ColorDialog1->Color;  //����� ���������� ����� ������ ���� �������
	Image2->Canvas->Brush->Color=newRect.color; //���������� ��������� ����� � ��������� ����
	Image2->Canvas->FillRect(Rect(0,0,Image2->Height,Image2->Width));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Image1MouseMove(TObject *Sender, TShiftState Shift, int X,   //�������� ����
          int Y)
{
	mousePos.x = X;      //���������� ������� ��������� ���� � ���������� ����������
	mousePos.y = Y;

	if(mode == Dmode::Drawing)
	{

	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
	newRect.first = mousePos;     //���������� ������� ���������� ���� � ���������� ������ ����� ���������� ����� ������
	int i = 0, j;    //���������� ������� ������� ��� ������������ ��������� ������ � �������

	History.resize(kCur);    //���������������� ������� � ������� ��������

	//��������� ������ �� �� � �����-������ ������
	for(auto rect : History)   //���������� ��� ������ � �������
	{
		bool strike = false;

		switch(rect.shape)
		{
			case shapes::Line:     //�������� �� �����
				if (((mousePos.x - rect.first.x)/(rect.second.x - rect.first.x)==(mousePos.y - rect.first.y)/(rect.second.y - rect.first.y))) strike = true;
				break;
			case shapes::Square:   //�������� �� �������������
				if ((rect.first.x <= mousePos.x && mousePos.x <= rect.second.x)
				&& ( rect.first.y <= mousePos.y && mousePos.y <= rect.second.y)) strike = true;
				break;
			case shapes::Circle:   //�������� �� ������
				if (1>=(pow(mousePos.x-0.5*(rect.first.x+rect.second.x), 2)/(pow((rect.first.x-rect.second.x), 2))
				 + (pow(mousePos.y-0.5*(rect.first.y+rect.second.y), 2)/(pow((rect.first.y-rect.second.y), 2))))) strike = true;
				break;
		}
		if(strike)         //���� ������
		{
			t = rect;      //��������� ������ � �����
			j = i;         //��������� ������� � �����
			mode = Dmode::Moveing;    //�������� ����� ��������������
		}
		i++;
	}

	if(mode == Dmode::Moveing)   //���� ��������� ��� ���������
	{
		History.erase(History.begin() +	j);   //������� ��������� ������ � �������, � ������� �� ������
		drawTo(History.size());               //�������������� �������� ��� ������
	}
	else mode = Dmode::Drawing;               //����� �������� ����� ���������
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image1MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	newRect.second = mousePos;    //���������� ������� ���������� ���� � ���������� ������ ����� ���������� ����� ������

	if(mode == Dmode::Moveing)    //���� ����� ����� ���������
	{
		cords d {newRect.second.x - newRect.first.x, newRect.second.y - newRect.first.y};  //����������� ��������� ��������� ����
		t.first.x += d.x;
		t.first.y += d.y;                      //��������� ��� ��������� � ��������������� ������
		t.second.x += d.x;
		t.second.y += d.y;
		draw(t);                  //������ �
		History.push_back(t);     //��������� � � �������
	}
	else
	{
		draw(newRect);       //������ ��������� ����� ������
		History.push_back(newRect);      //��������� ��� � �������
	}
	kCur = History.size();             //��������� ���������� �������
	mode = Dmode::None;                //������ ������� �����
}
//---------------------------------------------------------------------------

void __fastcall TForm1::RadioButton1Click(TObject *Sender)
{
	newRect.shape = shapes::Line;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::RadioButton2Click(TObject *Sender)
{
	newRect.shape = shapes::Square;           //�������� ������
}
//---------------------------------------------------------------------------

void __fastcall TForm1::RadioButton3Click(TObject *Sender)
{
	newRect.shape = shapes::Circle;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit1KeyPress(TObject *Sender, System::WideChar &Key)
{
    if((!isdigit(Key))&(Key!='\b')) Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit1Change(TObject *Sender)
{
	if(Edit1->Text=="") Edit1->Text = "0";     //������ �����
	newRect.Width = StrToInt(Edit1->Text);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image3Click(TObject *Sender)
{
	if(ColorDialog1->Execute()) newRect.backCol = ColorDialog1->Color;     //����� ���������� ����� ������ ���� �������
	Image3->Canvas->Brush->Color = newRect.backCol;                        //���������� ��������� ����� � ��������� ����
	Image3->Canvas->FillRect(Rect(0,0,Image3->Height,Image3->Width));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)     //��������
{
	Image1->Picture->Bitmap->FreeImage();                   //������� ��������
	Image1->Picture->Bitmap = NULL;
	kCur = 0;                                              //������� �������
	History.clear();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)       //���������
{
	if(SaveDialog1->Execute())
	{
		ofstream fout(SaveDialog1->FileName.w_str(), ios::binary);  //��������� �������� ����
		for(auto Rect: History)                                //���������� ��� ������ � �������
		{
			fout.write(reinterpret_cast<char*>(&Rect), sizeof(rect));  //���������� � ���� ������ ������
		}
		fout.close();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)    //���������
{
	rect tmp;                  //��������� ��������� �������� ������
	History.clear();           //������� �������
	if(OpenDialog1->Execute())
	{
		ifstream fin(OpenDialog1->FileName.w_str(), ios::binary);  //��������� �������� ����
		while(!fin.eof())    //������ ��� ���� �� ����� �����
		{
			fin.read(reinterpret_cast<char*>(&tmp), sizeof(rect)); //���������� � ����� ������ �������
			History.push_back(tmp);       //��������� � ������� ��������� �������� ������
		}

		kCur = History.size();      //��������� ���������� �������
		drawTo(kCur);       //�������������� �������� �� �������
	}
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button4Click(TObject *Sender)     //undo
{
	if(kCur==0) return;             //���� �� �� ������� ������� � �������, �� ������ �� ����������
	kCur--;                         //����� ���������� �� ���� �������
	drawTo(kCur);                   //� ��������������
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)    //redo
{
	if(kCur==History.size()) return;      //���� �� �� ��������� ������ � �������, �� ������ �� ����������
	kCur++;                               //����� ����������� �� ����
	drawTo(kCur);                         //� ��������������
}

void __fastcall TForm1::drawTo(int to)    //������������ �� �������� �������
{
	Image1->Picture->Bitmap->FreeImage();       //������� ��������
	Image1->Picture->Bitmap = NULL;

	for (auto Rect : History) draw(Rect);  //���������� ��� ������ � ������� � ������ ��
}
void __fastcall TForm1::draw(rect Rect)   //����������
{
	Image1->Canvas->Pen->Color = Rect.color;     //�������� �����
	Image1->Canvas->Pen->Width = Rect.Width;
	Image1->Canvas->Brush->Color = Rect.backCol;

	switch(Rect.shape)              //�������� ������ � ������
	{
		case shapes::Line:
            Image1->Canvas->MoveTo(Rect.first.x, Rect.first.y);
			Image1->Canvas->LineTo(Rect.second.x, Rect.second.y);
			break;
		case shapes::Square:
			Image1->Canvas->Rectangle(Rect.first.x, Rect.first.y, Rect.second.x, Rect.second.y);
			break;
		case shapes::Circle:
			Image1->Canvas->Ellipse(Rect.first.x, Rect.first.y, Rect.second.x, Rect.second.y);
			break;
	}
}
//---------------------------------------------------------------------------

