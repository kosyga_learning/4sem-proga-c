#include "pch.h"
#include "ProductList.h"
#include <fstream>
#include <ctime>

void ProductList::newProduct(System::String^ name, System::String^ number, System::String^ count, System::String^ price, System::String^ expiration, System::String^ temperature)
{
	Product* p;
	std::vector<std::string> time = split(ustoc(expiration),'.');
	if(temperature == "")
		p = new Indust(name, number, count, price);
	else
		p = new Grocery(name, number, count, price, date(std::stoi(time[0]), std::stoi(time[1]), std::stoi(time[2])), temperature);
	List.push_back(p);
}

std::vector<std::string> ProductList::split(std::string str, const char sep)
{
	std::vector<std::string> values(1);
	for each (char sym in str)
	{
		if (sym != sep) values[values.size() - 1] += sym;
		else values.resize(values.size() + 1);
	}
	return values;
}

int ProductList::totalCost()
{
	int totalCost = 0;
	for each (auto item in List)
		totalCost += item->getPrice() * item->getCount();
	return totalCost;
}

void ProductList::Show(System::Windows::Forms::DataGridView^ table)
{
	table->Rows->Clear();
	for each (auto item in List)
	{
		std::vector<std::string> values = item->getData();
		table->Rows->Add(gcnew System::String(values[0].c_str()), std::atoi(values[1].c_str()), std::atoi(values[2].c_str()), std::atoi(values[3].c_str()), gcnew System::String(values[4].c_str()), std::atoi(values[5].c_str()), std::atoi(values[2].c_str()) * std::atoi(values[3].c_str()));
	}
}
void ProductList::Serialize(System::String^ path)
{
	std::ofstream fout(Product::ustoc(path));
	for each (auto item in List)
	{
		std::vector<std::string> values = item->getData();
		fout << values[0] + separator + values[1] + separator + values[2] 
			+ separator + values[3] + separator + values[4] + separator + values[5] + "\n";
	}
	fout.close();
}
void ProductList::Load(System::String^ path)
{
	List.resize(0);
	std::ifstream fin(Product::ustoc(path));
	std::string str;
	while (std::getline(fin, str))
	{
		std::vector<std::string> values = split(str);
		ProductList::newProduct(gcnew System::String(values[0].c_str()),
			gcnew System::String(values[1].c_str()), gcnew System::String(values[2].c_str()),
			gcnew System::String(values[3].c_str()), gcnew System::String(values[4].c_str()), gcnew System::String(values[5].c_str()));
	}
	fin.close();
}

bool ProductList::Find(System::Windows::Forms::DataGridView^ table, System::String^ text, int col)
{
	bool isFaunded = false;
	table->ClearSelection();
	for(int i = 0; i < table->RowCount; i++)
	{
		if (table->Rows[i]->Cells[col]->Value->ToString() == text) 
		{
			table->Rows[i]->Selected = true;
			isFaunded = true;
		}
	}
	return isFaunded;
}