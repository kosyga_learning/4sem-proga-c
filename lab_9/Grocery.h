#include "Product.h"
#pragma once
class date
{
private:
	int day, month, year;
public:
	date(){}
	date(int day, int month, int year);
	std::string getValue();
};
class Grocery:public Product
{
protected:
	date expiration;
	int temperature;
public:
	Grocery():Product(){}
	Grocery(System::String^ name, System::String^ number, System::String^ count, System::String^ price, date expiration, System::String^ temperature);
	char* getName();
	int getNumber();
	int getCount();
	int getPrice();
	std::string getDate();
	int getTemperature();
	virtual std::vector<std::string> getData() override;
};


