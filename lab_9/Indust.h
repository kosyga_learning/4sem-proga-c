#include "Product.h"
#pragma once
class Indust:public Product
{
public:
	Indust(System::String^ name, System::String^ number, System::String^ count, System::String^ price):Product(name, number, count, price){}
	char* getName();
	int getNumber();
	int getCount();
	int getPrice();
	virtual std::vector<std::string> getData() override;
};

