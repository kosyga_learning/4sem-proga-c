#include "Grocery.h"
#include "Indust.h"
#pragma once

class ProductList:Product
{
private:
	std::vector<Product*> List;
	const char separator = '|';
	std::vector<std::string> split(std::string str, const char sep = '|');
public:
	ProductList(){}
	void newProduct(System::String^ name, System::String^ number, System::String^ count, System::String^ price, System::String^ expiration, System::String^ temperature);
	int totalCost();
	void Show(System::Windows::Forms::DataGridView ^table);
	void Serialize(System::String^ path);
	void Load(System::String^ path);
	bool Find(System::Windows::Forms::DataGridView^ table, System::String^ text, int col);
};

