#include "pch.h"
#include "Grocery.h"

date::date(int day, int month, int year)
{
	this->day = day;
	this->month = month;
	this->year = year;
}

std::string date::getValue()
{
	return std::to_string(day) + "." + std::to_string(month) + "." + std::to_string(year);
}

Grocery::Grocery(System::String^ name, System::String^ number, System::String^ count, System::String^ price, date expiration, System::String^ temperature):Product(name, number, count, price)
{
	this->expiration = expiration;
	this->temperature = (temperature == "") ? 0 : std::stoi(ustoc(temperature));
}

char* Grocery::getName() { return Product::getName(); }
int Grocery::getNumber() { return Product::getNumber(); }
int Grocery::getCount() { return Product::getCount(); }
int Grocery::getPrice() { return Product::getPrice(); }
std::string Grocery::getDate() { return expiration.getValue(); }
int Grocery::getTemperature() { return temperature; }

std::vector<std::string> Grocery::getData()
{
	std::vector<std::string> values = Product::getData();
	values[4] = getDate();
	values[5] = std::to_string(getTemperature());
	return values;
}