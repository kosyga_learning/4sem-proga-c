#include "pch.h"
#include "Indust.h"

char* Indust::getName() { return Product::getName(); }
int Indust::getNumber() { return Product::getNumber(); }
int Indust::getCount() { return Product::getCount(); }
int Indust::getPrice() { return Product::getPrice(); }

std::vector<std::string> Indust::getData()
{
	std::vector<std::string> values = Product::getData();
	return values;
}