#include <vector>
#include <string> 
#pragma once
using namespace System::Runtime::InteropServices;
class Product
{
protected:
	char *name;
	int number;
	int count;
	int price;

	char* ustoc(System::String^ str);

	Product(){}
	Product(System::String^ name, System::String^ number, System::String^ count, System::String^ price);
public:
	virtual std::vector<std::string> getData();
	char* getName();
	int getNumber();
	int getCount();
	int getPrice();

};

