#include "pch.h"
#include "Product.h"

char* Product::ustoc(System::String^ str)
{
	System::IntPtr ptrToNativeString = Marshal::StringToHGlobalAnsi(str); //������ AnsiString � MVS
	return static_cast<char*>(ptrToNativeString.ToPointer());
}

Product::Product(System::String^ name, System::String^ number, System::String^ count, System::String^ price)
{
	this->name = ustoc(name);
	this->number = std::stoi(ustoc(number));
	this->count = (count == "") ? 1 : std::stoi(ustoc(count));
	this->price = (price == "") ? 0 : std::stoi(ustoc(price));
}

char* Product::getName() { return name; }
int Product::getNumber() { return number; }
int Product::getCount() { return count; }
int Product::getPrice() { return price; }

std::vector<std::string> Product::getData()
{
	std::vector<std::string> values = { "","","","","","" };
	values[0] = getName();
	values[1] = std::to_string(getNumber());
	values[2] = std::to_string(getCount());
	values[3] = std::to_string(getPrice());
	return values;
}