#include "ProductList.h"
#include "Search.h"
#pragma once

namespace CppCLRWinFormsProject {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^ newProd;
	private: System::Windows::Forms::Button^ addButton;
	protected:

	private: System::Windows::Forms::TextBox^ temperature;
	private: System::Windows::Forms::Label^ label5;

	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::TextBox^ price;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::TextBox^ number;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::TextBox^ name;
	private: System::Windows::Forms::RadioButton^ isIndust;
	private: System::Windows::Forms::RadioButton^ isTrade;
	private: System::Windows::Forms::DateTimePicker^ expiration;
	private: System::Windows::Forms::DateTimePicker^ dateTimePicker1;


	private: System::Windows::Forms::Button^ getTotalCost;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::TextBox^ count;
	private: System::Windows::Forms::DataGridView^ dataGridView1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ nameCol;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ numberCol;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ countCol;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ priceCol;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ expirationCol;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ temperatureCol;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ totalCostCol;
	private: System::Windows::Forms::Button^ save;
	private: System::Windows::Forms::Button^ load;
	private: System::Windows::Forms::SaveFileDialog^ saveFileDialog1;
	private: System::Windows::Forms::OpenFileDialog^ openFileDialog1;
	private: System::Windows::Forms::ColorDialog^ colorDialog1;
	private: System::Windows::Forms::Button^ find;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->newProd = (gcnew System::Windows::Forms::GroupBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->count = (gcnew System::Windows::Forms::TextBox());
			this->expiration = (gcnew System::Windows::Forms::DateTimePicker());
			this->addButton = (gcnew System::Windows::Forms::Button());
			this->temperature = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->price = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->number = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->name = (gcnew System::Windows::Forms::TextBox());
			this->isIndust = (gcnew System::Windows::Forms::RadioButton());
			this->isTrade = (gcnew System::Windows::Forms::RadioButton());
			this->getTotalCost = (gcnew System::Windows::Forms::Button());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->nameCol = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->numberCol = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->countCol = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->priceCol = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->expirationCol = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->temperatureCol = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->totalCostCol = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->save = (gcnew System::Windows::Forms::Button());
			this->load = (gcnew System::Windows::Forms::Button());
			this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->colorDialog1 = (gcnew System::Windows::Forms::ColorDialog());
			this->find = (gcnew System::Windows::Forms::Button());
			this->newProd->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->SuspendLayout();
			// 
			// newProd
			// 
			this->newProd->Controls->Add(this->label6);
			this->newProd->Controls->Add(this->count);
			this->newProd->Controls->Add(this->expiration);
			this->newProd->Controls->Add(this->addButton);
			this->newProd->Controls->Add(this->temperature);
			this->newProd->Controls->Add(this->label5);
			this->newProd->Controls->Add(this->label4);
			this->newProd->Controls->Add(this->label3);
			this->newProd->Controls->Add(this->price);
			this->newProd->Controls->Add(this->label2);
			this->newProd->Controls->Add(this->number);
			this->newProd->Controls->Add(this->label1);
			this->newProd->Controls->Add(this->name);
			this->newProd->Controls->Add(this->isIndust);
			this->newProd->Controls->Add(this->isTrade);
			this->newProd->Location = System::Drawing::Point(13, 13);
			this->newProd->Name = L"newProd";
			this->newProd->Size = System::Drawing::Size(152, 351);
			this->newProd->TabIndex = 0;
			this->newProd->TabStop = false;
			this->newProd->Text = L"�����";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(46, 140);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(41, 13);
			this->label6->TabIndex = 15;
			this->label6->Text = L"���-��";
			// 
			// count
			// 
			this->count->Location = System::Drawing::Point(7, 156);
			this->count->Name = L"count";
			this->count->Size = System::Drawing::Size(124, 20);
			this->count->TabIndex = 14;
			this->count->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::count_KeyPress);
			// 
			// expiration
			// 
			this->expiration->Location = System::Drawing::Point(7, 234);
			this->expiration->Name = L"expiration";
			this->expiration->Size = System::Drawing::Size(124, 20);
			this->expiration->TabIndex = 13;
			// 
			// addButton
			// 
			this->addButton->Location = System::Drawing::Point(7, 305);
			this->addButton->Name = L"addButton";
			this->addButton->Size = System::Drawing::Size(124, 23);
			this->addButton->TabIndex = 12;
			this->addButton->Text = L"��������";
			this->addButton->UseVisualStyleBackColor = true;
			this->addButton->Click += gcnew System::EventHandler(this, &Form1::addButton_Click);
			// 
			// temperature
			// 
			this->temperature->Location = System::Drawing::Point(7, 279);
			this->temperature->Name = L"temperature";
			this->temperature->Size = System::Drawing::Size(124, 20);
			this->temperature->TabIndex = 11;
			this->temperature->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::temperature_KeyPress);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(7, 262);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(124, 13);
			this->label5->TabIndex = 10;
			this->label5->Text = L"����������� ��������";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(32, 218);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(82, 13);
			this->label4->TabIndex = 8;
			this->label4->Text = L"���� ��������";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(54, 179);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(33, 13);
			this->label3->TabIndex = 7;
			this->label3->Text = L"����";
			// 
			// price
			// 
			this->price->Location = System::Drawing::Point(7, 195);
			this->price->Name = L"price";
			this->price->Size = System::Drawing::Size(124, 20);
			this->price->TabIndex = 6;
			this->price->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::price_KeyPress);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(46, 102);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(41, 13);
			this->label2->TabIndex = 5;
			this->label2->Text = L"�����";
			// 
			// number
			// 
			this->number->Location = System::Drawing::Point(7, 118);
			this->number->Name = L"number";
			this->number->Size = System::Drawing::Size(124, 20);
			this->number->TabIndex = 4;
			this->number->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::number_KeyPress);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(43, 66);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(57, 13);
			this->label1->TabIndex = 3;
			this->label1->Text = L"��������";
			// 
			// name
			// 
			this->name->Location = System::Drawing::Point(7, 82);
			this->name->Name = L"name";
			this->name->Size = System::Drawing::Size(124, 20);
			this->name->TabIndex = 2;
			// 
			// isIndust
			// 
			this->isIndust->AutoSize = true;
			this->isIndust->Location = System::Drawing::Point(7, 44);
			this->isIndust->Name = L"isIndust";
			this->isIndust->Size = System::Drawing::Size(107, 17);
			this->isIndust->TabIndex = 1;
			this->isIndust->Text = L"������������";
			this->isIndust->UseVisualStyleBackColor = true;
			this->isIndust->CheckedChanged += gcnew System::EventHandler(this, &Form1::isIndust_CheckedChanged);
			// 
			// isTrade
			// 
			this->isTrade->AutoSize = true;
			this->isTrade->Checked = true;
			this->isTrade->Location = System::Drawing::Point(7, 20);
			this->isTrade->Name = L"isTrade";
			this->isTrade->Size = System::Drawing::Size(93, 17);
			this->isTrade->TabIndex = 0;
			this->isTrade->TabStop = true;
			this->isTrade->Text = L"�����������";
			this->isTrade->UseVisualStyleBackColor = true;
			// 
			// getTotalCost
			// 
			this->getTotalCost->Location = System::Drawing::Point(171, 341);
			this->getTotalCost->Name = L"getTotalCost";
			this->getTotalCost->Size = System::Drawing::Size(128, 23);
			this->getTotalCost->TabIndex = 3;
			this->getTotalCost->Text = L"����� ���������";
			this->getTotalCost->UseVisualStyleBackColor = true;
			this->getTotalCost->Click += gcnew System::EventHandler(this, &Form1::getTotalCost_Click);
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(7) {
				this->nameCol,
					this->numberCol, this->countCol, this->priceCol, this->expirationCol, this->temperatureCol, this->totalCostCol
			});
			this->dataGridView1->Location = System::Drawing::Point(171, 16);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->RowHeadersVisible = false;
			this->dataGridView1->Size = System::Drawing::Size(509, 319);
			this->dataGridView1->TabIndex = 4;
			// 
			// nameCol
			// 
			this->nameCol->HeaderText = L"��������";
			this->nameCol->Name = L"nameCol";
			this->nameCol->ReadOnly = true;
			// 
			// numberCol
			// 
			this->numberCol->HeaderText = L"�����";
			this->numberCol->Name = L"numberCol";
			this->numberCol->ReadOnly = true;
			// 
			// countCol
			// 
			this->countCol->HeaderText = L"���-��";
			this->countCol->Name = L"countCol";
			this->countCol->ReadOnly = true;
			// 
			// priceCol
			// 
			this->priceCol->HeaderText = L"����";
			this->priceCol->Name = L"priceCol";
			this->priceCol->ReadOnly = true;
			// 
			// expirationCol
			// 
			this->expirationCol->HeaderText = L"���� ��������";
			this->expirationCol->Name = L"expirationCol";
			this->expirationCol->ReadOnly = true;
			// 
			// temperatureCol
			// 
			this->temperatureCol->HeaderText = L"����������� ��������";
			this->temperatureCol->Name = L"temperatureCol";
			this->temperatureCol->ReadOnly = true;
			// 
			// totalCostCol
			// 
			this->totalCostCol->HeaderText = L"����� ���������";
			this->totalCostCol->Name = L"totalCostCol";
			this->totalCostCol->ReadOnly = true;
			// 
			// save
			// 
			this->save->Location = System::Drawing::Point(305, 341);
			this->save->Name = L"save";
			this->save->Size = System::Drawing::Size(125, 23);
			this->save->TabIndex = 5;
			this->save->Text = L"���������";
			this->save->UseVisualStyleBackColor = true;
			this->save->Click += gcnew System::EventHandler(this, &Form1::save_Click);
			// 
			// load
			// 
			this->load->Location = System::Drawing::Point(436, 341);
			this->load->Name = L"load";
			this->load->Size = System::Drawing::Size(104, 23);
			this->load->TabIndex = 6;
			this->load->Text = L"���������";
			this->load->UseVisualStyleBackColor = true;
			this->load->Click += gcnew System::EventHandler(this, &Form1::load_Click);
			// 
			// saveFileDialog1
			// 
			this->saveFileDialog1->Filter = L"��������� ����� (*.txt)|*.txt|��� ����� (*.*)|*.*";
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			this->openFileDialog1->Filter = L"��������� ����� (*.txt)|*.txt|��� ����� (*.*)|*.*";
			// 
			// find
			// 
			this->find->Location = System::Drawing::Point(547, 340);
			this->find->Name = L"find";
			this->find->Size = System::Drawing::Size(133, 23);
			this->find->TabIndex = 7;
			this->find->Text = L"�����";
			this->find->UseVisualStyleBackColor = true;
			this->find->Click += gcnew System::EventHandler(this, &Form1::find_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(692, 375);
			this->Controls->Add(this->find);
			this->Controls->Add(this->load);
			this->Controls->Add(this->save);
			this->Controls->Add(this->dataGridView1);
			this->Controls->Add(this->getTotalCost);
			this->Controls->Add(this->newProd);
			this->Name = L"Form1";
			this->Text = L"������";
			this->newProd->ResumeLayout(false);
			this->newProd->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	ProductList *PList = new ProductList;
	private: int onlyDigits(System::Windows::Forms::KeyPressEventArgs^ e)
	{
		if ((!isdigit(e->KeyChar)) && (e->KeyChar != '\b')) return 0;
		else return e->KeyChar;
	}
	private: System::Void isIndust_CheckedChanged(System::Object^ sender, System::EventArgs^ e) 
	{
		expiration->Enabled = !expiration->Enabled;
		label4->Enabled = expiration->Enabled;
		temperature->Enabled = !temperature->Enabled;
		label5->Enabled = temperature->Enabled;
	}
	private: System::Void addButton_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		if(name->Text=="")
		{
			MessageBox::Show("�� �� ����� ��������");
			return;
		}
		if (number->Text == "")
		{
			MessageBox::Show("�� �� ����� ����� ������");
			return;
		}
		System::String^ date = expiration->Value.Day + "." + expiration->Value.Month + "." + expiration->Value.Year;
		PList->newProduct(name->Text, number->Text, count->Text, price->Text, date, temperature->Text);
		PList->Show(dataGridView1);
	}
	private: System::Void getTotalCost_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		MessageBox::Show("����� ��������� = " + PList->totalCost() + " ���");
	}
	private: System::Void number_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		e->KeyChar = onlyDigits(e);
	}
	private: System::Void count_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		e->KeyChar = onlyDigits(e);
	}
	private: System::Void price_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		e->KeyChar = onlyDigits(e);
	}
	private: System::Void temperature_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		e->KeyChar = onlyDigits(e);
	}
	private: System::Void save_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		saveFileDialog1->ShowDialog();
		if (saveFileDialog1->FileName)
			PList->Serialize(saveFileDialog1->FileName);
	}
	private: System::Void load_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		openFileDialog1->ShowDialog();
		if (openFileDialog1->FileName)
		{
			dataGridView1->Rows->Clear();
			PList->Load(openFileDialog1->FileName);
		}
		PList->Show(dataGridView1);
	}
	private: System::Void find_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		Search^ s = gcnew Search();
		s->StartSearch(PList, dataGridView1);
		s->Show();
	}
};
}
