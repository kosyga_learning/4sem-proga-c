#pragma once
#include <string.h>

using namespace System::Runtime::InteropServices;
class Stroka
{
private:
	static const short int len = 128;
	char str[len];
public:
	Stroka(){strcpy(str,"\0"); };
	Stroka(String^ s)
	{
		IntPtr ptrToNativeString = Marshal::StringToHGlobalAnsi(s); //������ AnsiString � MVS
		char* ch = static_cast<char*>(ptrToNativeString.ToPointer());
		strcpy(str, ch); 
	};
	operator char* () { return str; }
};