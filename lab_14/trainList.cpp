//---------------------------------------------------------------------------
#pragma hdrstop
#include <string>
#include "trainList.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

std::vector<String> trainList::split(String str)
{
	std::vector<String> words(1);
	char *str1 = new char[str.Length()+1];
	strcpy(str1, AnsiString(str).c_str());
	for(int i = 0; i < str.Length(); i++)
	{
		if(str1[i] != '|')
			words[words.size()-1] += AnsiString(str1[i]);
		else words.resize(words.size()+1);
	}
	return words;
}

void trainList::addTrain(AnsiString way, int count, AnsiString date, AnsiString time)
{
	this->TList.push_back(train(way, count, date, time));
}
void trainList::updateTable(TStringGrid *table)
{
	for (unsigned int i = 1; i < this->TList.size()+1; i++) {
		table->RowCount = i+1;
		table->Cells[0][i] = this->TList[i-1].getWay();
		table->Cells[1][i] = this->TList[i-1].getCount();
		table->Cells[2][i] = this->TList[i-1].getDate();
		table->Cells[3][i] = this->TList[i-1].getTime();
	}
}
void trainList::Serialize(String FileName) {
	char *path = AnsiString(FileName).c_str();
	std::ofstream fout(path, ios::out);
	for(unsigned int i = 0; i < this->TList.size(); i++){
		fout << TList[i].getWay() << "|" << TList[i].getCount()	<< "|" << TList[i].getDate() << "|" << TList[i].getTime() << "\n";
	}
	fout.close();
}

void trainList::Load(String FileName) {
	TStringList *file = new TStringList;
    file->LoadFromFile(FileName);
	for(int i = 0; i < file->Count; i++){
		std::vector<String> Strings = split(file->Strings[i]);
		addTrain(Strings[0], StrToInt(Strings[1]), Strings[2], Strings[3]);
	}
}

bool trainList::compareTime(AnsiString t1, AnsiString t2)
{
	TDateTime time1 = TDateTime(t1,TDateTime::Time);
	TDateTime time2 = TDateTime(t2,TDateTime::Time);
	return time1 <= time2;
}

bool trainList::Reserve(AnsiString way, int count, AnsiString date, AnsiString time)
{
	int code = 0;
	AnsiString res[4] = {"��� ������ � ����� " + way, "��� ������ � ����� " + way + " �� " + date, "������ ���� ������������ ����� ��� " + time, "�� ������ ���� �� ���������� ����"};
	for(unsigned int i = 0; i < this->TList.size(); i++)
	{
		if(TList[i].getWay() == way)
			if(TList[i].getDate() == date)
				if(compareTime(TList[i].getTime(), time))
					if(TList[i].getCount() >= count){
						if(MessageDlg(L"������������ �������� �� " + TList[i].getTime() + "\n�������������?", mtInformation, TMsgDlgButtons() << mbYes << mbNo, 0) == mrYes){
                            TList[i].setCount(TList[i].getCount()-count);
							return true;
						}
						return false;
					}
					else code = std::max(code, 3);
				else code = std::max(code, 2);
			else code = std::max(code, 1);
	}
	ShowMessage(res[code]);
	return false;
}

train trainList::getTrain(int i)
{
    return TList[i];
}
