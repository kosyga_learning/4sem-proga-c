object Form1: TForm1
  Left = 0
  Top = 0
  Caption = #1055#1086#1077#1079#1076#1072
  ClientHeight = 441
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  TextHeight = 15
  object Label1: TLabel
    Left = 199
    Top = 11
    Width = 68
    Height = 15
    Alignment = taCenter
    Caption = #1050#1086#1083'-'#1074#1086' '#1084#1077#1089#1090
  end
  object Label2: TLabel
    Left = 320
    Top = 11
    Width = 99
    Height = 15
    Caption = #1044#1072#1090#1072' '#1086#1090#1087#1088#1072#1074#1083#1077#1085#1080#1103
  end
  object Label3: TLabel
    Left = 464
    Top = 11
    Width = 109
    Height = 15
    Caption = #1042#1088#1077#1084#1103' '#1086#1090#1087#1088#1072#1074#1083#1077#1085#1080#1103
  end
  object DateTimePicker1: TDateTimePicker
    Left = 312
    Top = 32
    Width = 121
    Height = 23
    Date = 45085.000000000000000000
    Time = 0.888859814811439700
    TabOrder = 0
  end
  object TimePicker1: TTimePicker
    Left = 439
    Top = 32
    Width = 181
    Height = 25
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    TabOrder = 1
    Time = 45085.888981863420000000
    TimeFormat = 'h:nn'
  end
  object Way: TLabeledEdit
    Left = 8
    Top = 32
    Width = 138
    Height = 23
    Alignment = taCenter
    EditLabel.Width = 100
    EditLabel.Height = 15
    EditLabel.Caption = #1055#1091#1085#1082#1090' '#1085#1072#1079#1085#1072#1095#1077#1085#1080#1103
    TabOrder = 2
    Text = ''
    OnChange = WayChange
  end
  object Count: TNumberBox
    Left = 152
    Top = 32
    Width = 154
    Height = 23
    Alignment = taCenter
    MaxValue = 1000.000000000000000000
    TabOrder = 3
  end
  object Add: TButton
    Left = 8
    Top = 61
    Width = 298
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 4
    OnClick = AddClick
  end
  object Clear: TButton
    Left = 312
    Top = 61
    Width = 308
    Height = 25
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    TabOrder = 5
    OnClick = ClearClick
  end
  object StringGrid1: TStringGrid
    Left = 8
    Top = 92
    Width = 612
    Height = 317
    BorderStyle = bsNone
    ColCount = 4
    DefaultColWidth = 152
    DefaultColAlignment = taCenter
    FixedColor = clWhite
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri Light'
    Font.Style = [fsBold]
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSizing, goColSizing, goRowSelect, goFixedRowDefAlign]
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 6
    OnSelectCell = StringGrid1SelectCell
  end
  object Save: TButton
    Left = 8
    Top = 415
    Width = 185
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    TabOrder = 7
    OnClick = SaveClick
  end
  object Load: TButton
    Left = 199
    Top = 415
    Width = 234
    Height = 25
    Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
    TabOrder = 8
    OnClick = LoadClick
  end
  object Reserve: TButton
    Left = 439
    Top = 415
    Width = 181
    Height = 25
    Caption = #1047#1072#1088#1077#1079#1077#1088#1074#1080#1088#1086#1074#1072#1090#1100
    TabOrder = 9
    OnClick = ReserveClick
  end
  object OpenDialog1: TOpenDialog
    Filter = '|*.txt'
    Left = 408
    Top = 280
  end
  object SaveDialog1: TSaveDialog
    Filter = '|*.txt'
    Left = 472
    Top = 280
  end
end
