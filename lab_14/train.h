//---------------------------------------------------------------------------
#include <vcl.h>
#include <iostream.h>
#include <fstream.h>
#ifndef trainH
#define trainH
//---------------------------------------------------------------------------
#endif

class train{
private:
	AnsiString way;
	int count;
    TDateTime dateTime;
public:
	train(){}
	train(AnsiString way, int count, AnsiString date, AnsiString time);
	AnsiString getWay(){ return this->way; }
	int getCount(){ return this->count; }
	AnsiString getDate(){ return this->dateTime.DateString(); }
	AnsiString getTime(){ return this->dateTime.TimeString(); }
    void setCount(int count){ this->count = count;	}
};