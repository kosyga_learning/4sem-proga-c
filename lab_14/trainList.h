//---------------------------------------------------------------------------
#include "train.h"
#include <vector>
#include <Vcl.Grids.hpp>
#include <fstream>
#ifndef trainListH
#define trainListH
//---------------------------------------------------------------------------
#endif
class trainList{
private:
	std::vector<train> TList;
	bool compareTime(AnsiString t1, AnsiString t2);
public:
    trainList(){ }
	void addTrain(AnsiString way, int count, AnsiString date, AnsiString time);
	void updateTable(TStringGrid *table);
	void clear(){ this->TList.resize(0); }
	void Serialize(String FileName);
	void Load(String FileName);
	bool Reserve(AnsiString way, int count, AnsiString date, AnsiString time);
	train getTrain(int i);
	std::vector<String> split(String str);
};
