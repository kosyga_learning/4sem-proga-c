//---------------------------------------------------------------------------

#include <vfw.h>
#include <System.Classes.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.Mask.hpp>
#include <Vcl.NumberBox.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.WinXPickers.hpp>
#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.Mask.hpp>
#include <Vcl.NumberBox.hpp>
#include <Vcl.WinXPickers.hpp>
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TDateTimePicker *DateTimePicker1;
	TTimePicker *TimePicker1;
	TLabeledEdit *Way;
	TNumberBox *Count;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TButton *Add;
	TButton *Clear;
	TStringGrid *StringGrid1;
	TButton *Save;
	TButton *Load;
	TOpenDialog *OpenDialog1;
	TSaveDialog *SaveDialog1;
	TButton *Reserve;
	void __fastcall tableClear();
    void __fastcall playSound(const wchar_t *sound);
	void __fastcall AddClick(TObject *Sender);
	void __fastcall ClearClick(TObject *Sender);
	void __fastcall SaveClick(TObject *Sender);
	void __fastcall LoadClick(TObject *Sender);
	void __fastcall ReserveClick(TObject *Sender);
	void __fastcall StringGrid1SelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
	void __fastcall WayChange(TObject *Sender);
	bool __fastcall compareWay(String way, String w);
	char __fastcall toLower(char ch);
	void __fastcall search();
    void __fastcall reverseTable();

private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};

class TPublicGrid: public  TStringGrid
{
    public:
        using TStringGrid::DeleteRow;
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
