//---------------------------------------------------------------------------
#include "trainList.h"
#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
trainList *List = new trainList();
bool state = false;
bool sorts[] = {false,false,false,false};
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	StringGrid1->Cells[0][0] = "����� ����������";
	StringGrid1->Cells[1][0] = "���-�� ��������� ����";
	StringGrid1->Cells[2][0] = "���� �����������";
	StringGrid1->Cells[3][0] = "����� �����������";
}
//---------------------------------------------------------------------------
char __fastcall TForm1::toLower(char ch)
{
	if(ch>='A' && ch<='Z') ch+='z'-'Z';
	if(ch>='�' && ch<='�') ch+='�'-'�';
	return ch;
}

bool __fastcall TForm1::compareWay(String way, String w)
{
	for(int i = 0; i < std::min(way.Length(), w.Length()); i++)
		if(toLower(AnsiString(w).c_str()[i]) != toLower(AnsiString(way).c_str()[i]))
			return false;
    return true;
}
void __fastcall TForm1::search()
{
	int j = 1;
	do if(!compareWay(Way->Text, StringGrid1->Cells[0][j]))
		{
			if(StringGrid1->RowCount == 1) return;
			((TPublicGrid*)StringGrid1)->DeleteRow(j);
		}
		else j++;
	while(j < StringGrid1->RowCount);
}
void __fastcall TForm1::tableClear()
{
    StringGrid1->RowCount = 1;
    List->clear();
}
void __fastcall TForm1::playSound(const wchar_t *sound)
{
    HWND MCIHwnd;
	MCIHwnd = MCIWndCreate(Application->Handle, HInstance, NULL, sound);
	MCIWndPlay(MCIHwnd);
}
void __fastcall TForm1::AddClick(TObject *Sender)
{
	if(Way->Text == "")
	{
        ShowMessage("�� �� ����� �������!");
		return;
	}
    playSound(L"..\\..\\train.wav");
	List->addTrain(Way->Text, Count->ValueInt, DateToStr(DateTimePicker1->Date), TimeToStr(TimePicker1->Time));
	List->updateTable(StringGrid1);
	Way->Clear();
	Count->Text = 0;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ClearClick(TObject *Sender)
{
	tableClear();
	Way->Clear();
    Count->ValueInt = 0;
    playSound(L"..\\..\\clear.mp3");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::SaveClick(TObject *Sender)
{
    if(SaveDialog1->Execute()){
		List->Serialize(SaveDialog1->FileName);
        playSound(L"..\\..\\write.mp3");
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::LoadClick(TObject *Sender)
{
	if (OpenDialog1 -> Execute()) {
		tableClear();
		List->Load(OpenDialog1->FileName);
		List->updateTable(StringGrid1);
		playSound(L"..\\..\\loaded.mp3");
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ReserveClick(TObject *Sender)
{
	if(List->Reserve(Way->Text, Count->ValueInt, DateToStr(DateTimePicker1->Date), TimeToStr(TimePicker1->Time))){
        Way->Clear();
		List->updateTable(StringGrid1);
        Way->Clear();
		playSound(L"..\\..\\reserve.mp3");
	}
}

void __fastcall TForm1::reverseTable()
{
	TStringGrid *buff = new TStringGrid(Form1);
	int j = StringGrid1->RowCount-1;
	buff->RowCount = j+1;
	for(int i = 0; i < StringGrid1->RowCount; i++)
	{
		buff->Rows[i] = StringGrid1->Rows[j];
        j--;
	}
	for(int i = 1; i < StringGrid1->RowCount; i++)
        StringGrid1->Rows[i] = buff->Rows[i-1];
}

void __fastcall TForm1::StringGrid1SelectCell(TObject *Sender, int ACol, int ARow,
		  bool &CanSelect)
{
	if(ARow == 0)
	{
		switch(ACol)
		{
			case 0:
				{
					TStringList *lst  = new TStringList;
					TStringList *lst1 = new TStringList;

					for(int i=1;i<StringGrid1->RowCount;++i)    //�������� ��� � ���� � ����� �������������� ���� ���������� ����� ������, ��� �� ����� ��� �����������
					lst->Add(StringGrid1->Cells[ACol][i]+"~"+IntToStr(i));

					lst->Sorted = true;

					for(int i=0;i<StringGrid1->RowCount;++i)   //������ �������� �������,��� �� �� ��� ����� ����������� ��������
					lst1->Add(StringGrid1->Rows[i]->Text);

					for(int i=0;i<lst->Count;++i)     //������ �������������� �������
					StringGrid1->Rows[i+1]->DelimitedText =lst1->Strings[lst->Strings[i].SubString(lst->Strings[i].Pos("~") + 1,lst->Strings[i].Length()).ToInt()]  ;

					delete lst,lst1;
				}
				break;
			case 1:
				for (int j = 1; j<StringGrid1->RowCount; j++)
					for (int i = 1; i<StringGrid1->RowCount-j; i++)
						if (StrToInt(StringGrid1->Cells[ACol][i])>StrToInt(StringGrid1->Cells[ACol][i+1]))
						{
							StringGrid1->Rows[StringGrid1->RowCount]=StringGrid1->Rows[i+1];
							StringGrid1->Rows[i+1]= StringGrid1->Rows[i];
							StringGrid1->Rows[i]=StringGrid1->Rows[StringGrid1->RowCount];
						}
				break;
			case 2:
				for (int j = 1; j<StringGrid1->RowCount; j++)
					for (int i = 1; i<StringGrid1->RowCount-j; i++)
						if (TDateTime(StringGrid1->Cells[ACol][i],TDateTime::Date)>TDateTime(StringGrid1->Cells[ACol][i+1],TDateTime::Date))
						{
							StringGrid1->Rows[StringGrid1->RowCount]=StringGrid1->Rows[i+1];
							StringGrid1->Rows[i+1]= StringGrid1->Rows[i];
							StringGrid1->Rows[i]=StringGrid1->Rows[StringGrid1->RowCount];
						}
				break;
			case 3:
				for (int j = 1; j<StringGrid1->RowCount; j++)
					for (int i = 1; i<StringGrid1->RowCount-j; i++)
						if (TDateTime(StringGrid1->Cells[ACol][i],TDateTime::Time)>TDateTime(StringGrid1->Cells[ACol][i+1],TDateTime::Time))
						{
							StringGrid1->Rows[StringGrid1->RowCount]=StringGrid1->Rows[i+1];
							StringGrid1->Rows[i+1]= StringGrid1->Rows[i];
							StringGrid1->Rows[i]=StringGrid1->Rows[StringGrid1->RowCount];
						}
				break;
		}
		if(sorts[ACol])
		{
			reverseTable();
        }
		sorts[ACol]=!sorts[ACol];
		playSound(L"..\\..\\list.mp3");
	}
	else
		if(!state)
			{
				state = true;
				Way->Text = StringGrid1->Cells[0][ARow];
				DateTimePicker1->Date = TDateTime(StringGrid1->Cells[2][ARow],TDateTime::Date);
				TimePicker1->Time = TDateTime(StringGrid1->Cells[3][ARow],TDateTime::Time);
				state = false;
			}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::WayChange(TObject *Sender)
{
    if(!state)
	{
		state = true;
		List->updateTable(StringGrid1);
		if(Way->Text != "")
			search();
		state = false;
	}
}
//---------------------------------------------------------------------------

