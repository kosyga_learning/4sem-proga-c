#include <fstream>
#pragma once
struct stack
{
	char b[500];
	int i = -1;
	void push(char sym) {
		i++;
		b[i] = sym;
	}
	void pop() {
		i--;
	}
	char top() {
		char ch = b[i];
		return ch;
	}
	bool empty() {
		return i == -1;
	}
	stack() {};
	stack(char* path)
	{
		std::ifstream fin(path);
		if (fin.is_open())
			do {
				char ch = fin.get();
				push(ch);
			} while (!fin.eof());
		fin.close();
	}
};