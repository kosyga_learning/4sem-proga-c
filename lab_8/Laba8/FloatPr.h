#pragma once
#include "Float.h"
class FloatPr: public Float
{
public:
	using Float::getStr;
	FloatPr(System::String^ value) : Float(value) {}
	FloatPr(Float f) : Float(f) {};
	FloatPr operator+(FloatPr number) { return Float::operator+(number); }
	FloatPr operator-(FloatPr number) { return this->value - number.value; }
	FloatPr operator*(FloatPr number) { return this->value * number.value; }
	FloatPr operator/(FloatPr number) { return this->value / number.value; }
};

