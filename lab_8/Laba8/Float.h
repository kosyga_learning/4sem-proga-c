#pragma once
#include <iostream>
#include <string>
#include <string.h>
#include <cstdlib>
#include <cmath>

using namespace System::Runtime::InteropServices;
class Float
{
private:
	char* ustoc(System::String^ str)
	{
		System::IntPtr ptrToNativeString = Marshal::StringToHGlobalAnsi(str); //������ AnsiString � MVS
		return static_cast<char*>(ptrToNativeString.ToPointer());
	}
protected:
	float value;
public:
	Float() { this->value = 0; }
	Float(float value) { this->value = value; }
	Float(System::String^ value) { this->value = std::stof(ustoc(value)); }
	System::String^ getStr() { return gcnew System::String(std::to_string(this->value).c_str()); }
	float getValue() { return this->value; }
	Float operator+(Float number) { return this->value + number.value; }
};

