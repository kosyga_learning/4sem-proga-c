#pragma once
#include "FloatPr.h"

namespace CppCLRWinFormsProject {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^ Num1;
	protected:

	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::TextBox^ Num2;

	private: System::Windows::Forms::Button^ Plus;
	private: System::Windows::Forms::Button^ Minus;
	private: System::Windows::Forms::Button^ Multiply;
	private: System::Windows::Forms::Button^ Devide;
	private: System::Windows::Forms::TextBox^ Answer;




	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->Num1 = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->Num2 = (gcnew System::Windows::Forms::TextBox());
			this->Plus = (gcnew System::Windows::Forms::Button());
			this->Minus = (gcnew System::Windows::Forms::Button());
			this->Multiply = (gcnew System::Windows::Forms::Button());
			this->Devide = (gcnew System::Windows::Forms::Button());
			this->Answer = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// Num1
			// 
			this->Num1->Location = System::Drawing::Point(12, 28);
			this->Num1->Name = L"Num1";
			this->Num1->Size = System::Drawing::Size(100, 20);
			this->Num1->TabIndex = 0;
			this->Num1->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->Num1->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::Num1_KeyPress);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(13, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(77, 13);
			this->label1->TabIndex = 1;
			this->label1->Text = L"������ �����";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(13, 73);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(75, 13);
			this->label2->TabIndex = 3;
			this->label2->Text = L"������ �����";
			// 
			// Num2
			// 
			this->Num2->Location = System::Drawing::Point(12, 92);
			this->Num2->Name = L"Num2";
			this->Num2->Size = System::Drawing::Size(100, 20);
			this->Num2->TabIndex = 2;
			this->Num2->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->Num2->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::Num2_KeyPress);
			// 
			// Plus
			// 
			this->Plus->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Plus->Location = System::Drawing::Point(128, 28);
			this->Plus->Name = L"Plus";
			this->Plus->Size = System::Drawing::Size(43, 43);
			this->Plus->TabIndex = 4;
			this->Plus->Text = L"+";
			this->Plus->UseVisualStyleBackColor = true;
			this->Plus->Click += gcnew System::EventHandler(this, &Form1::Plus_Click);
			// 
			// Minus
			// 
			this->Minus->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Minus->Location = System::Drawing::Point(197, 28);
			this->Minus->Name = L"Minus";
			this->Minus->Size = System::Drawing::Size(43, 43);
			this->Minus->TabIndex = 5;
			this->Minus->Text = L"-";
			this->Minus->UseVisualStyleBackColor = true;
			this->Minus->Click += gcnew System::EventHandler(this, &Form1::Minus_Click);
			// 
			// Multiply
			// 
			this->Multiply->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Multiply->Location = System::Drawing::Point(128, 77);
			this->Multiply->Name = L"Multiply";
			this->Multiply->Size = System::Drawing::Size(43, 43);
			this->Multiply->TabIndex = 6;
			this->Multiply->Text = L"*";
			this->Multiply->UseVisualStyleBackColor = true;
			this->Multiply->Click += gcnew System::EventHandler(this, &Form1::Multiply_Click);
			// 
			// Devide
			// 
			this->Devide->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Devide->Location = System::Drawing::Point(197, 77);
			this->Devide->Name = L"Devide";
			this->Devide->Size = System::Drawing::Size(43, 43);
			this->Devide->TabIndex = 7;
			this->Devide->Text = L"/";
			this->Devide->UseVisualStyleBackColor = true;
			this->Devide->Click += gcnew System::EventHandler(this, &Form1::Devide_Click);
			// 
			// Answer
			// 
			this->Answer->Location = System::Drawing::Point(16, 176);
			this->Answer->Name = L"Answer";
			this->Answer->ReadOnly = true;
			this->Answer->Size = System::Drawing::Size(228, 20);
			this->Answer->TabIndex = 8;
			this->Answer->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->Answer);
			this->Controls->Add(this->Devide);
			this->Controls->Add(this->Multiply);
			this->Controls->Add(this->Minus);
			this->Controls->Add(this->Plus);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->Num2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->Num1);
			this->Name = L"Form1";
			this->Text = L"Laba8";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: void onlyFloat(System::Windows::Forms::KeyPressEventArgs^ e)
	{
		if ((!isdigit(e->KeyChar)) && (e->KeyChar != '.')&&(e->KeyChar!='\b')) e->KeyChar = 0;
	}
	private: System::Void Num1_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		onlyFloat(e);
		if (Num1->TextLength > 0)
		{
			if ((Num1->Text[Num1->TextLength - 1] == '.') && (e->KeyChar == '.')) e->KeyChar = 0;
			if (Num1->TextLength > 1)
				if ((Num1->Text[Num1->TextLength - 2] == '.') && (e->KeyChar != '\b')) e->KeyChar = 0;
		}
		else if (e->KeyChar == '.') e->KeyChar = 0;
	}
	private: System::Void Num2_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		onlyFloat(e);
		if (Num2->TextLength > 0)
		{
			if ((Num2->Text[Num2->TextLength - 1] == '.') && (e->KeyChar == '.')) e->KeyChar = 0;
			if (Num2->TextLength > 1)
				if ((Num2->Text[Num2->TextLength - 2] == '.') && (e->KeyChar != '\b')) e->KeyChar = 0;
		}
		else if (e->KeyChar == '.') e->KeyChar = 0;
	}
	private: System::Void Plus_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		Answer->Text = FloatPr(FloatPr(Num1->Text) + FloatPr(Num2->Text)).getStr();
	}
	private: System::Void Minus_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		Answer->Text = FloatPr(FloatPr(Num1->Text) - FloatPr(Num2->Text)).getStr();
	}
	private: System::Void Multiply_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		Answer->Text = FloatPr(FloatPr(Num1->Text) * FloatPr(Num2->Text)).getStr();
	}
	private: System::Void Devide_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		Answer->Text = FloatPr(FloatPr(Num1->Text) / FloatPr(Num2->Text)).getStr();
	}
};
}
