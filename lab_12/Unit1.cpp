//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TZ *Z;
//---------------------------------------------------------------------------
__fastcall TZ::TZ(TComponent* Owner)
	: TForm3D(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TZ::StrokeCube1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y, TVector3D &RayPos, TVector3D &RayDir)
{
	FDown = ::PointF(X, Y);
}
//---------------------------------------------------------------------------
void __fastcall TZ::StrokeCube1MouseMove(TObject *Sender, TShiftState Shift, float X,
          float Y, TVector3D &RayPos, TVector3D &RayDir)
{
	if (Shift.Contains(ssLeft)) {
	  StrokeCube1->RotationAngle->X = StrokeCube1->RotationAngle->X - ((Y - FDown.Y) * 0.3);
	  StrokeCube1->RotationAngle->Y = StrokeCube1->RotationAngle->Y + ((X - FDown.X) * 0.3);

	  FDown = ::PointF(X, Y);
	}
	else if(Shift.Contains(ssRight)){
		StrokeCube1->Position->X = (X - Z->Width/2)/30;
		StrokeCube1->Position->Y = (Y - Z->Height/2)/30;
    }
}
//---------------------------------------------------------------------------


void __fastcall TZ::EarthMouseMove(TObject *Sender, TShiftState Shift, float X, float Y,
          TVector3D &RayPos, TVector3D &RayDir)
{
	if (Shift.Contains(ssRight)) {
		Earth->Position->X = (X - Z->Width/2)/30;
		Earth->Position->Y = (Y - Z->Height/2)/30;
	}
}
//---------------------------------------------------------------------------

