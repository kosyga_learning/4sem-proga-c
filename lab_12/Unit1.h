//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Forms3D.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Controls3D.hpp>
#include <FMX.Objects3D.hpp>
#include <FMX.Types.hpp>
#include <System.Math.Vectors.hpp>
#include <FMX.MaterialSources.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Layers3D.hpp>
//---------------------------------------------------------------------------
class TZ : public TForm3D
{
__published:	// IDE-managed Components
	TSphere *Earth;
	TFloatAnimation *FloatAnimation1;
	TStrokeCube *StrokeCube1;
	TFloatAnimation *FloatAnimation2;
	TImage3D *Image3D1;
	TFloatAnimation *FloatAnimation3;
	TTextureMaterialSource *TextureMaterialSource1;
	TTextureMaterialSource *TextureMaterialSource2;
	TSphere *Moon;
	TFloatAnimation *FloatAnimation4;
	TFloatAnimation *FloatAnimation5;
	TFloatAnimation *FloatAnimation6;
	void __fastcall StrokeCube1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  float X, float Y, TVector3D &RayPos, TVector3D &RayDir);
	void __fastcall StrokeCube1MouseMove(TObject *Sender, TShiftState Shift, float X,
		  float Y, TVector3D &RayPos, TVector3D &RayDir);
	void __fastcall EarthMouseMove(TObject *Sender, TShiftState Shift, float X, float Y,
          TVector3D &RayPos, TVector3D &RayDir);
private:	// User declarations
	TPointF FDown;
public:		// User declarations
	__fastcall TZ(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TZ *Z;
//---------------------------------------------------------------------------
#endif
