//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
std::vector<int> array;
Smode mode;
SYSTEMTIME st1, st2;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Memo1KeyPress(TObject *Sender, System::WideChar &Key)
{
	if((!isdigit(Key))&(Key!='\b')) Key = 0;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	AnsiString text;
	for(int i = 0; i <20000; i++)
	{
		array.push_back(rand()%1000);
		text += IntToStr(array[array.size()-1]) + "\r\n";
	}
	Memo1->Lines->Add(text);
    Button2->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	ProgressBar1->Position = 0;
    ProgressBar1->Max = 20000;
	Memo2->Clear();
	mode = Smode(RadioGroup1->ItemIndex);
	GetLocalTime(&st1);
	switch(mode){
		case Smode::Bubble:
			BubbleSort(array);
			break;
		case Smode::Shaker:
			ShakerSort(array);
			break;
		case Smode::Comb:
			CombSort(array);
			break;
		case Smode::Insert:
			InsertionSort(array);
			break;
		case Smode::Select:
			SelectionSort(array);
			break;
		case Smode::Quick:
			QuickSort(array);
			break;
		case Smode::Merge:
			MergeSort(array);
			break;
		case Smode::Heap:
			HeapSort(array);
			break;
	}
	GetLocalTime(&st2);
	AnsiString text;
	for(auto num : array)
		text+= IntToStr(num) + "\r\n";
	Memo2->Lines->Add(text);
	double time = (st2.wMinute*60*1000 + st2.wSecond*1000 + st2.wMilliseconds)-(st1.wMinute*60*1000 + st1.wSecond*1000 + st1.wMilliseconds);
	AnsiString answer = "����� ����������:" + FloatToStr(time) + "(ms).";
	Memo3->Lines->Add(answer);
}
//---------------------------------------------------------------------------
