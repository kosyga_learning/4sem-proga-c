object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 441
  ClientWidth = 898
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  TextHeight = 15
  object GroupBox1: TGroupBox
    Left = 37
    Top = 23
    Width = 185
    Height = 409
    Caption = #1048#1089#1093#1086#1076#1085#1099#1081' '#1084#1072#1089#1089#1080#1074
    TabOrder = 0
    object Memo1: TMemo
      Left = 3
      Top = 16
      Width = 179
      Height = 361
      ScrollBars = ssVertical
      TabOrder = 0
      OnKeyPress = Memo1KeyPress
    end
    object Button1: TButton
      Left = 3
      Top = 381
      Width = 179
      Height = 25
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100
      TabOrder = 1
      OnClick = Button1Click
    end
  end
  object GroupBox2: TGroupBox
    Left = 377
    Top = 24
    Width = 185
    Height = 409
    Caption = #1054#1090#1089#1086#1088#1090#1080#1088#1086#1074#1072#1085#1085#1099#1081' '#1084#1072#1089#1089#1080#1074
    TabOrder = 1
    object Memo2: TMemo
      Left = 3
      Top = 16
      Width = 179
      Height = 361
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
    object Button2: TButton
      Left = 3
      Top = 381
      Width = 179
      Height = 25
      Caption = #1054#1090#1089#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100
      Enabled = False
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object Memo3: TMemo
    Left = 576
    Top = 24
    Width = 314
    Height = 377
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 2
  end
  object ProgressBar1: TProgressBar
    Left = 576
    Top = 407
    Width = 314
    Height = 26
    TabOrder = 3
  end
  object RadioGroup1: TRadioGroup
    Left = 225
    Top = 24
    Width = 146
    Height = 409
    Caption = 'RadioGroup1'
    Items.Strings = (
      #1055#1091#1079#1099#1088#1100#1082#1086#1074#1072#1103
      #1064#1077#1081#1082#1077#1088#1085#1072#1103
      #1056#1072#1089#1095#1105#1089#1082#1086#1081
      #1042#1089#1090#1072#1074#1082#1072#1084#1080
      #1042#1099#1073#1086#1088#1086#1084
      #1041#1099#1089#1090#1088#1072#1103
      #1057#1083#1080#1103#1085#1080#1077#1084
      #1055#1080#1088#1072#1084#1080#1076#1086#1080#1076#1072#1083#1100#1085#1072#1103)
    TabOrder = 4
  end
end
