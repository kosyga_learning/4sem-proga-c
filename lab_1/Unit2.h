//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
#include <vector>
#include <iterator>
#include <algorithm>
//---------------------------------------------------------------------------
using std::vector;
void BubbleSort(vector<int>& values);
void ShakerSort(vector<int>& values);
void CombSort(vector<int>& values);
void InsertionSort(vector<int>& values);
void SelectionSort(vector<int>& values);
void QuickSort(vector<int>& values);
void MergeSort(vector<int>& values);
void HeapSort(vector<int>& values);
#endif

