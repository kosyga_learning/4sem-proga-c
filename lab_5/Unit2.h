//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#endif
class MyNumber
{
	public:
		double Num;
		MyNumber() {Num = 0;}
		MyNumber(double Number) {Num = Number;}
		MyNumber operator+(MyNumber Num2)
		{
			return(Num + Num2.Num);
		}
		MyNumber operator-(MyNumber Num2)
		{
			return(Num - Num2.Num);
		}
		MyNumber operator*(MyNumber Num2)
		{
			return(Num * Num2.Num);
		}
		MyNumber operator/(MyNumber Num2)
		{
			return(Num / Num2.Num);
		}
		MyNumber operator>(MyNumber Num2)
		{
			return(Num > Num2.Num);
		}
		MyNumber operator==(MyNumber Num2)
		{
			return(Num == Num2.Num);
		}
};