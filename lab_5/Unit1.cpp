//---------------------------------------------------------------------------
#include <cctype>
#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::LabeledEdit1KeyPress(TObject *Sender, System::WideChar &Key)

{
	if((!std::isdigit(Key))&&(Key != '\b')&&(Key != '.')) Key = 0;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	MyNumber Num = StrToInt(LabeledEdit1->Text);
	MyNumber Num2 = StrToInt(LabeledEdit2->Text);
	MyNumber Num3 = Num+Num2;
	ShowMessage(AnsiString(Num3.Num));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
    MyNumber Num = StrToInt(LabeledEdit1->Text);
	MyNumber Num2 = StrToInt(LabeledEdit2->Text);
	MyNumber Num3 = Num-Num2;
	ShowMessage(AnsiString(Num3.Num));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
    MyNumber Num = StrToInt(LabeledEdit1->Text);
	MyNumber Num2 = StrToInt(LabeledEdit2->Text);
	MyNumber Num3 = Num/Num2;
	ShowMessage(AnsiString(Num3.Num));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{
    MyNumber Num = StrToInt(LabeledEdit1->Text);
	MyNumber Num2 = StrToInt(LabeledEdit2->Text);
	MyNumber Num3 = Num*Num2;
	ShowMessage(AnsiString(Num3.Num));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button5Click(TObject *Sender)
{
	MyNumber Num = StrToInt(LabeledEdit1->Text);
	MyNumber Num2 = StrToInt(LabeledEdit2->Text);
	MyNumber Num3 = Num>Num2;
	ShowMessage(AnsiString(Num3.Num));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button6Click(TObject *Sender)
{
   MyNumber Num = StrToInt(LabeledEdit1->Text);
	MyNumber Num2 = StrToInt(LabeledEdit2->Text);
	MyNumber Num3 = Num==Num2;
	ShowMessage(AnsiString(Num3.Num));
}
//---------------------------------------------------------------------------
